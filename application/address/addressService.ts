import AddressRepository from "@/infrastructure/address/addressRepository";
import { Address, IAddress } from "@/domain/address/address";
import { signOut } from "next-auth/react";

type ApiResponse<T> = Promise<{ data: T; error: null }>;

interface IAddAddress {
  accountUuid: string;
  address: string;
  phone: string;
  postalCode: string;
  state: string;
  country: string;
}

class AddressService {
  private static instance: AddressService;
  private addressRepository: AddressRepository;

  constructor(addressRepository: AddressRepository) {
    this.addressRepository = addressRepository;
  }

  static getInstance(addressRepository: AddressRepository): AddressService {
    if (!AddressService.instance) {
      AddressService.instance = new AddressService(addressRepository);
    }
    return AddressService.instance;
  }

  deleteAddress(accountUuid: string, addressUuid: string): ApiResponse<string> {
    return this.addressRepository
      .deleteAddress(accountUuid, addressUuid)
      .then((data: any) => {
        return data.match({
          Ok: () => {
            return { data, error: null };
          },
          Err: (error: any): any => {
            if (error?.response?.status === 401) {
              signOut();
            }
            return {
              data: [],
              error: error?.response?.data || {
                statusCode: 500,
                message: error.message,
              },
            };
          },
        });
      });
  }

  activeAddress(accountUuid: string, addressUuid: string): ApiResponse<string> {
    return this.addressRepository
      .activeAddress(accountUuid, addressUuid)
      .then((response: any) => {
        return response.match({
          Ok: (data: any) => {
            return { data, error: null };
          },
          Err: (error: any): any => {
            if (error?.response?.status === 401) {
              signOut();
            }
            return {
              data: [],
              error: error?.response?.data || {
                statusCode: 500,
                message: error.message,
              },
            };
          },
        });
      });
  }

  getAddresses(accountUuid: string): ApiResponse<IAddress[]> {
    return this.addressRepository
      .getAddresses(accountUuid)
      .then((addressClass) => {
        return addressClass.match({
          Ok: (addresses: IAddress[]) => {
            const data: IAddress[] = addresses.map((address) =>
              new Address({
                uuid: address.uuid,
                active: address.active,
                address: address.address,
                phone: address.phone,
                postalCode: address.postalCode,
                country: address.country,
                state: address.state,
              }).getData()
            );
            return { data, error: null };
          },
          Err: (error: any): any => {
            if (error?.response?.status === 401) {
              signOut();
            }

            return {
              data: [],
              error: error?.response?.data || {
                statusCode: 500,
                message: error.message,
              },
            };
          },
        });
      });
  }

  addAddress({
    accountUuid,
    address,
    phone,
    postalCode,
    state,
    country,
  }: IAddAddress): ApiResponse<IAddress[]> {
    return this.addressRepository
      .addAddress({ accountUuid, address, phone, postalCode, state, country })
      .then((addressClass) => {
        return addressClass.match({
          Ok: (addresses: IAddress[]) => {
            const data: IAddress[] = addresses.map((address) =>
              new Address({
                uuid: address.uuid,
                active: address.active,
                address: address.address,
                phone: address.phone,
                postalCode: address.postalCode,
                country: address.country,
                state: address.state,
              }).getData()
            );
            return { data, error: null };
          },
          Err: (error: any): any => {
            if (error?.response?.status === 401) {
              signOut();
            }

            return {
              data: [],
              error: error?.response?.data || {
                statusCode: 500,
                message: error.message,
              },
            };
          },
        });
      });
  }
}

export default AddressService;
