import AuthRepository from "@/infrastructure/auth/authRepository";
import { showToastify } from "@/features/general/generalSlice";
import { store } from "@/store";

type ApiResponse<T> = Promise<{
  data: T;
  error: null;
  Ok: boolean;
}>;

class AuthService {
  private static instance: AuthService;
  private authRepository: AuthRepository;

  constructor(authRepository: AuthRepository) {
    this.authRepository = authRepository;
  }

  static getInstance(authRepository: AuthRepository): AuthService {
    if (!AuthService.instance) {
      AuthService.instance = new AuthService(authRepository);
    }
    return AuthService.instance;
  }

  signIn(email: string, password: string): ApiResponse<string> {
    return this.authRepository.signIn(email, password).then((user) => {
      return user.match({
        Ok: (data: string) => {
          return { data, error: null, Ok: true };
        },
        Err: (error: any): any => {
          const errorMessage = error?.response?.data?.message || error?.message;

          store.dispatch(showToastify({ mode: "error", text: errorMessage }));

          return {
            Ok: false,
            data: [],
            error: error?.response?.data || {
              statusCode: 500,
              message: errorMessage,
            },
          };
        },
      });
    });
  }
  signUp(email: string, password: string, name: string): ApiResponse<string> {
    return this.authRepository.signUp(email, password, name).then((message) => {
      return message.match({
        Ok: (data: string) => {
          return { data, error: null, Ok: true };
        },
        Err: (error: any): any => {
          const errorMessage = error?.response?.data?.message || error?.message;

          store.dispatch(showToastify({ mode: "error", text: errorMessage }));
          return {
            data: [],
            ok: false,
            error: error?.response?.data || {
              statusCode: 500,
              message: error.message,
            },
          };
        },
      });
    });
  }
}

export default AuthService;
