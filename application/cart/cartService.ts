import CartRepository from "@/infrastructure/cart/cartRepository";
import { Cart, ICart } from "@/domain/cart/cart";
import { signOut } from "next-auth/react";

type ApiResponse<T> = Promise<{ data: T; error: null }>;

interface IAddCart {
  accountUuid: string;
  productUuid: string;
  quantity: number;
  totallPrice: number;
}

interface IdeletedCart {
  accountUuid: string;
  productUuid: string;
}

class CartService {
  private static instance: CartService;
  private cartRepository: CartRepository;

  constructor(cartRepository: CartRepository) {
    this.cartRepository = cartRepository;
  }

  static getInstance(cartRepository: CartRepository): CartService {
    if (!CartService.instance) {
      CartService.instance = new CartService(cartRepository);
    }
    return CartService.instance;
  }

  getCarts(accountUuid: string): ApiResponse<ICart[]> {
    return this.cartRepository.getCarts(accountUuid).then((cartClasses) => {
      return cartClasses.match({
        Ok: (carts: ICart[]) => {
          const data: ICart[] = carts.map((cart) =>
            new Cart({
              uuid: cart.uuid,
              name: cart.name,
              image: cart.image,
              quantity: cart.quantity,
              totallPrice: cart.totallPrice,
            }).getData()
          );
          return { data, error: null };
        },
        Err: (error: any): any => {
          if (error?.response?.status === 401) {
            signOut();
          }

          return {
            data: [],
            error: error?.response?.data || {
              statusCode: 500,
              message: error.message,
            },
          };
        },
      });
    });
  }

  deleteCart({ accountUuid, productUuid }: IdeletedCart): ApiResponse<string> {
    return this.cartRepository
      .deleteCart({ accountUuid, productUuid })
      .then((data: any) => {
        return data.match({
          Ok: () => {
            return { data, error: null };
          },
          Err: (error: any): any => {
            if (error?.response?.status === 401) {
              signOut();
            }
            return {
              data: [],
              error: error?.response?.data || {
                statusCode: 500,
                message: error.message,
              },
            };
          },
        });
      });
  }

  deleteCarts(deleteCarts: string[]): ApiResponse<string> {
    return this.cartRepository.deleteCarts(deleteCarts).then((data: any) => {
      return data.match({
        Ok: () => {
          return { data, error: null };
        },
        Err: (error: any): any => {
          if (error?.response?.status === 401) {
            signOut();
          }
          return {
            data: [],
            error: error?.response?.data || {
              statusCode: 500,
              message: error.message,
            },
          };
        },
      });
    });
  }

  deleteCartByUuid(uuid: string): ApiResponse<string> {
    return this.cartRepository.deleteCartByUuid(uuid).then((data: any) => {
      return data.match({
        Ok: () => {
          return { data, error: null };
        },
        Err: (error: any): any => {
          if (error?.response?.status === 401) {
            signOut();
          }
          return {
            data: [],
            error: error?.response?.data || {
              statusCode: 500,
              message: error.message,
            },
          };
        },
      });
    });
  }

  addCart({
    accountUuid,
    productUuid,
    quantity,
    totallPrice,
  }: IAddCart): ApiResponse<ICart[]> {
    return this.cartRepository
      .addCart({ accountUuid, productUuid, quantity, totallPrice })
      .then((data: any) => {
        return data.match({
          Ok: () => {
            return { data, error: null };
          },
          Err: (error: any): any => {
            if (error?.response?.status === 401) {
              signOut();
            }
            return {
              data: [],
              error: error?.response?.data || {
                statusCode: 500,
                message: error.message,
              },
            };
          },
        });
      });
  }
}

export default CartService;
