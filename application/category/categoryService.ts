import CategoryRepository from "@/infrastructure/category/categoryRepository";
import { Category, ICategory } from "@/domain/category/category";

type ApiResponse<T> = Promise<{ data: T; error: null }>;

class CategoryService {
  private static instance: CategoryService;
  private categoryRepository: CategoryRepository;

  constructor(categoryRepository: CategoryRepository) {
    this.categoryRepository = categoryRepository;
  }

  static getInstance(categoryRepository: CategoryRepository): CategoryService {
    if (!CategoryService.instance) {
      CategoryService.instance = new CategoryService(categoryRepository);
    }
    return CategoryService.instance;
  }

  getCategories(): ApiResponse<ICategory[]> {
    return this.categoryRepository.getCategories().then((categoriesClasses) => {
      return categoriesClasses.match({
        Ok: (categories: ICategory[]) => {
          const data: ICategory[] = categories.map((category) =>
            new Category({
              uuid: category.uuid,
              name: category.name,
              image: category.image,
            }).getData()
          );
          return { data, error: null };
        },
        Err: (error: any): any => {
          return {
            data: [],
            error: error?.response?.data || {
              statusCode: 500,
              message: error.message,
            },
          };
        },
      });
    });
  }
}

export default CategoryService;
