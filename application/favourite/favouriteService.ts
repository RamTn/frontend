import FavouriteRepository from "@/infrastructure/favourite/favouriteRepository";
import { IProduct, Product } from "@/domain/product/product";
import { signOut } from "next-auth/react";

type ApiResponse<T> = Promise<{ data: T; error: null; Ok: boolean }>;

class FavouriteService {
  private static instance: FavouriteService;
  private favouriteRepository: FavouriteRepository;

  constructor(favouriteRepository: FavouriteRepository) {
    this.favouriteRepository = favouriteRepository;
  }

  static getInstance(
    favouriteRepository: FavouriteRepository
  ): FavouriteService {
    if (!FavouriteService.instance) {
      FavouriteService.instance = new FavouriteService(favouriteRepository);
    }
    return FavouriteService.instance;
  }

  getAllFavouritesByAccountUuid(
    uuid: string,
    token: string
  ): ApiResponse<IProduct[]> {
    return this.favouriteRepository
      .getAllFavouritesByAccountUuid(uuid, token)
      .then((productsClasses) => {
        return productsClasses.match({
          Ok: (products: IProduct[]) => {
            const data: IProduct[] = products.map((product) =>
              new Product({
                uuid: product.uuid,
                name: product.name,
                description: product.description,
                price: product.price,
                averageRating: product.averageRating,
                category: product.category,
                image: product.image,
                subCategory: product.subCategory,
                isLiked: product.isLiked,
                isAddedToCart: product.isAddedToCart,
              }).getData()
            );
            return { data, error: null, Ok: true };
          },
          Err: (error: any): any => {
            if (error?.response?.status === 401) {
              signOut();
            }
            return {
              Ok: false,
              data: [],
              error: error?.response?.data || {
                statusCode: 500,
                message: error.message,
              },
            };
          },
        });
      });
  }

  addFavourite(productUuid: string, accountUuid: string): ApiResponse<string> {
    return this.favouriteRepository
      .addFavoutite(productUuid, accountUuid)
      .then((favouriteClasses) => {
        return favouriteClasses.match({
          Ok: (response: string) => {
            return { data: response, error: null, Ok: true };
          },
          Err: (error: any): any => {
            if (error?.response?.status === 401) {
              signOut();
            }
            return {
              Ok: false,
              data: [],
              error: error?.response?.data || {
                statusCode: 500,
                message: error.message,
              },
            };
          },
        });
      });
  }
}

export default FavouriteService;
