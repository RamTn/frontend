import CategoryRepository from "@/infrastructure/category/categoryRepository";
import CategoryService from "./category/categoryService";

import ProductRepository from "@/infrastructure/product/productRepository";
import ProductService from "./product/productService";

import AuthService from "./auth/authService";
import AuthRepository from "@/infrastructure/auth/authRepository";

import FavouriteRepository from "@/infrastructure/favourite/favouriteRepository";
import FavouriteService from "./favourite/favouriteService";

import CartRepository from "@/infrastructure/cart/cartRepository";
import CartService from "./cart/cartService";

import RatingRepository from "@/infrastructure/rating/ratingRepository";
import RatingService from "./rating/ratingService";

import AddressRepository from "@/infrastructure/address/addressRepository";
import AddressService from "./address/addressService";

export enum ApiTypes {
  CATEGORY = "CATEGORY",
  PRODUCT = "PRODUCT",
  AUTHENTICATION = "AUTHENTICATION",
  FAVOURITE = "FAVOURITE",
  CART = "CART",
  RATING = "RATING",
  ADDRESS = "ADDRESS",
}

type TServices =
  | CategoryService
  | ProductService
  | AuthService
  | FavouriteService
  | CartService
  | RatingService
  | AddressService;

export class ApiFactory {
  private static instance: ApiFactory;

  private constructor() {}

  static getInstance(): ApiFactory {
    if (!ApiFactory.instance) {
      ApiFactory.instance = new ApiFactory();
    }
    return ApiFactory.instance;
  }
  static createApi(type: ApiTypes): TServices {
    if (type in ApiTypes) {
      return {
        [ApiTypes.CATEGORY]: new CategoryService(new CategoryRepository()),
        [ApiTypes.PRODUCT]: new ProductService(new ProductRepository()),
        [ApiTypes.AUTHENTICATION]: new AuthService(new AuthRepository()),
        [ApiTypes.FAVOURITE]: new FavouriteService(new FavouriteRepository()),
        [ApiTypes.CART]: new CartService(new CartRepository()),
        [ApiTypes.RATING]: new RatingService(new RatingRepository()),
        [ApiTypes.ADDRESS]: new AddressService(new AddressRepository()),
      }[type];
    } else {
      throw new Error("Invalid API type");
    }
  }
}
