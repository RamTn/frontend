import ProductRepository from "@/infrastructure/product/productRepository";
import { Product, IProduct } from "@/domain/product/product";
import { signOut } from "next-auth/react";

type ApiResponse<T> = Promise<{ data: T; error: null }>;

class ProductService {
  constructor(private productRepository: ProductRepository) {}

  getAllProducts(
    query: NodeJS.Dict<string | string[]>
  ): ApiResponse<IProduct[]> {
    return this.productRepository
      .getAllProducts(query)
      .then((productsClasses) => {
        return productsClasses.match({
          Ok: (products: IProduct[]) => {
            const data: IProduct[] = products.map((product) =>
              new Product({
                uuid: product.uuid,
                name: product.name,
                description: product.description,
                price: product.price,
                averageRating: product.averageRating,
                category: product.category,
                image: product.image,
                subCategory: product.subCategory,
                isLiked: product.isLiked,
                isAddedToCart: product.isAddedToCart,
              }).getData()
            );

            return { data, error: null };
          },
          Err: (error: any): any => {
            return {
              data: [],
              error: error?.response?.data || {
                statusCode: 500,
                message: error.message,
              },
            };
          },
        });
      });
  }
  getPopulars(param: number, accountUuid?: string): ApiResponse<IProduct[]> {
    return this.productRepository
      .getPropulars(param, accountUuid)
      .then((productsClasses) => {
        return productsClasses.match({
          Ok: (products: IProduct[]) => {
            const data: IProduct[] = products.map((product) =>
              new Product({
                uuid: product.uuid,
                name: product.name,
                description: product.description,
                price: product.price,
                averageRating: product.averageRating,
                category: product.category,
                image: product.image,
                subCategory: product.subCategory,
                isLiked: product.isLiked,
                isAddedToCart: product.isAddedToCart,
                totallPrice: product.totallPrice,
                quantity: product.quantity,
              }).getData()
            );

            return { data, error: null };
          },
          Err: (error: any): any => {
            return {
              data: [],
              error: error?.response?.data || {
                statusCode: 500,
                message: error.message,
              },
            };
          },
        });
      });
  }
  getProductByUuid(
    ProductUuid: string,
    accountUuid?: string
  ): ApiResponse<IProduct> {
    return this.productRepository
      .getProductByUuid(ProductUuid, accountUuid)
      .then((productClasse) => {
        return productClasse.match({
          Ok: (product: IProduct) => {
            const data: IProduct = new Product({
              uuid: product.uuid,
              name: product.name,
              description: product.description,
              price: product.price,
              averageRating: product.averageRating,
              category: product.category,
              image: product.image,
              subCategory: product.subCategory,
              isLiked: product.isLiked,
              totallPrice: product.totallPrice,
              quantity: product.quantity,
              isAddedToCart: product.isAddedToCart,
            }).getData();

            return { data, error: null };
          },
          Err: (error: any): any => {
            if (error?.response?.status === 401) {
              signOut();
            }
            return {
              data: null,
              error: error?.response?.data || {
                statusCode: 500,
                message: error.message,
              },
            };
          },
        });
      });
  }

  searchProductsByName(
    query: NodeJS.Dict<string | string[]>
  ): ApiResponse<IProduct[]> {
    return this.productRepository
      .searchProductsByName(query)
      .then((productsClasses) => {
        return productsClasses.match({
          Ok: (products: IProduct[]) => {
            const data: IProduct[] = products.map((product) =>
              new Product({
                uuid: product.uuid,
                name: product.name,
                description: product.description,
                price: product.price,
                averageRating: product.averageRating,
                category: product.category,
                image: product.image,
                subCategory: product.subCategory,
                isLiked: product.isLiked,
                isAddedToCart: product.isAddedToCart,
              }).getData()
            );
            return { data, error: null };
          },
          Err: (error: any): any => {
            return {
              data: [],
              error: error?.response?.data || {
                statusCode: 500,
                message: error.message,
              },
            };
          },
        });
      });
  }
}

export default ProductService;
