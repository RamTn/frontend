import RatingRepository from "@/infrastructure/rating/ratingRepository";
import { signOut } from "next-auth/react";

type ApiResponse<T> = Promise<{ data: T; error: null }>;

interface IAddRating {
  productUuid: string;
  accountUuid: string;
  ratingValue: number;
}

class RatingService {
  private static instance: RatingService;
  private ratingRepository: RatingRepository;

  constructor(ratingRepository: RatingRepository) {
    this.ratingRepository = ratingRepository;
  }

  static getInstance(ratingRepository: RatingRepository): RatingService {
    if (!RatingService.instance) {
      RatingService.instance = new RatingService(ratingRepository);
    }
    return RatingService.instance;
  }

  addRating({
    accountUuid,
    productUuid,
    ratingValue,
  }: IAddRating): ApiResponse<number> {
    return this.ratingRepository
      .addRating({ accountUuid, ratingValue, productUuid })
      .then((ratingClasses) => {
        return ratingClasses.match({
          Ok: (avrRating) => {
            return { data: avrRating, error: null };
          },
          Err: (error: any): any => {
            if (error?.response?.status === 401) {
              signOut();
            }
            return {
              data: [],
              error: error?.response?.data || {
                statusCode: 500,
                message: error.message,
              },
            };
          },
        });
      });
  }
}

export default RatingService;
