import React, { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
// libraries
// Apis
import { ApiFactory, ApiTypes } from "@/application";
import AddressService from "@/application/address/addressService";
import { IAddress } from "@/domain/address/address";
import { AiOutlineCheckSquare, AiOutlineCloseCircle } from "react-icons/ai";

export interface IAddressProps {
  address: IAddress;
  deleteAddress: (value: string) => void;
  getActivateAddressUuid: (value: string) => void;
  activateAddressUuid: string;
}

export const Address: React.FC<IAddressProps> = ({
  address,
  deleteAddress,
  getActivateAddressUuid,
  activateAddressUuid,
}) => {
  //---------------------------- hooks --------------------------//
  const { data: session } = useSession();
  const [isChecked, setIsChecked] = useState(address.active);
  //------------------------ side effects -----------------------//
  useEffect(() => {
    if (activateAddressUuid !== address.uuid) setIsChecked(false);
    else setIsChecked(true);
  }, [activateAddressUuid]);
  //-------------------------- Actions --------------------------//
  const handleCheck = async (uuid: string) => {
    await (ApiFactory.createApi(ApiTypes.ADDRESS) as AddressService)
      .activeAddress(session?.user.uuid!, uuid)
      .then((_) => {
        getActivateAddressUuid(uuid);
      });
  };
  const handleDeleteCart = async (uuid: string) => {
    await (ApiFactory.createApi(ApiTypes.ADDRESS) as AddressService)
      .deleteAddress(session?.user.uuid!, uuid)
      .then((_) => {
        deleteAddress(uuid);
      });
  };
  //-------------------------------------------------------------//
  return (
    <div className="flex justify-between h-20 mt-7 drop-shadow-lg bg-gray-50 items-center rounded-md mb-4 w-full">
      <div className="flex pl-5 w-1/3">
        <div className="text-gray-600">{`${address.address}, ${address.postalCode}, ${address.state}, ${address.country}`}</div>
      </div>
      <div className="text-gray-600 w-1/3">{address.phone}</div>
      <AiOutlineCheckSquare
        onClick={() => handleCheck(address.uuid)}
        className={`cursor-pointer mr-3 ${
          isChecked ? "text-teal-400" : "text-gray-500"
        }`}
        size={30}
      />
      <AiOutlineCloseCircle
        onClick={() => handleDeleteCart(address.uuid)}
        size={25}
        className="mr-3 text-red-500 cursor-pointer"
      />
    </div>
  );
};

export default Address;
