import { useState } from "react";
import SignIn from "./signIn";
import SignUp from "./singnUp";

const Authentication = () => {
  const [isSignIn, setIsSignIn] = useState<boolean>(false);
  return !isSignIn ? (
    <SignIn setIsSignIn={setIsSignIn} />
  ) : (
    <SignUp setIsSignIn={setIsSignIn} />
  );
};

export default Authentication;
