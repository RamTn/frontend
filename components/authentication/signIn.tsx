// libraries
import React, { Dispatch, SetStateAction, useState } from "react";
import { GoMail } from "react-icons/go";
import { RiLockPasswordLine } from "react-icons/ri";
import { signIn } from "next-auth/react";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
// Apis
import { showToastify } from "@/features/general/generalSlice";
// Components
import Button from "../button/button";
import Input from "@/components/input/input";
import { useAppDispatch } from "@/store/hooks";

type TInputs = {
  email: string;
  password: string;
};

interface ISignIn {
  setIsSignIn: Dispatch<SetStateAction<boolean>>;
}

const SignIn: React.FC<ISignIn> = ({ setIsSignIn }) => {
  /*--------------------------- hooks --------------------------- */
  const [loading, setLoading] = useState<boolean>(false);
  const router = useRouter();
  const dispatch = useAppDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<TInputs>();
  /*---------------------- submit handlers ----------------------- */
  const switchToSignUp = () => setIsSignIn(true);
  const onSubmit = async (data: TInputs) => {
    setLoading(true);
    await signIn("credentials", {
      username: data.email,
      password: data.password,
      redirect: false,
    }).then((res) => {
      if (res?.ok) {
        router.push("/");
      } else {
        dispatch(
          showToastify({ mode: "error", text: "Email or Password is wrong" })
        );
      }
    });
    setLoading(false);
  };
  /*-------------------------------------------------------------- */
  return (
    <form className="w-96 p-8" onSubmit={handleSubmit(onSubmit)} noValidate>
      <Input
        error={errors?.email?.message}
        register={register("email", {
          required: "Email is required",
          pattern: {
            value: /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/,
            message: "Email is invalid",
          },
        })}
        type="email"
        label="Email"
        IconComponent={GoMail}
        placeholder="Enter your email"
      />
      <Input
        error={errors?.password?.message}
        register={register("password", {
          required: "Password is required",
        })}
        type="password"
        label="Password"
        IconComponent={RiLockPasswordLine}
        placeholder="Enter your password"
      />
      <div className="mt-10">
        <Button
          outline={false}
          handleClick={() => {}}
          name="sign in"
          type="submit"
          loading={loading}
        />
      </div>
      <p
        onClick={switchToSignUp}
        className="text-xs cursor-pointer text-gray-400 mt-2 text-center"
      >
        Sign up
      </p>
    </form>
  );
};

export default SignIn;
