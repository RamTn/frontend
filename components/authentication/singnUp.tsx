// libraries
import React, {
  Dispatch,
  SetStateAction,
  useEffect,
  useRef,
  useState,
} from "react";
import { useForm } from "react-hook-form";
import { GoMail } from "react-icons/go";
import { MdOutlineDriveFileRenameOutline } from "react-icons/md";
import { RiLockPasswordLine } from "react-icons/ri";
// Apis
import { ApiFactory, ApiTypes } from "@/application";
import AuthService from "@/application/auth/authService";
import { showToastify } from "@/features/general/generalSlice";
// Components
import Input from "@/components/input/input";
import Button from "../button/button";
import { useAppDispatch } from "@/store/hooks";

type TInputs = {
  email: string;
  password: string;
  name: string;
};

interface ISignUp {
  setIsSignIn: Dispatch<SetStateAction<boolean>>;
}

const SignUp: React.FC<ISignUp> = ({ setIsSignIn }) => {
  /*--------------------------- hooks --------------------------- */
  const [loading, setLoading] = useState<boolean>(false);
  const dispatch = useAppDispatch();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<TInputs>();
  /*---------------------- clearing timeout ---------------------- */
  let timer = useRef<any>();

  useEffect(() => {
    return () => {
      clearTimeout(timer.current);
    };
  }, [timer]);
  /*---------------------- submit handlers ----------------------- */
  const switchToSignIn = () => setIsSignIn(false);

  const onSubmit = async (data: TInputs) => {
    setLoading(true);

    const signUpResponse = await (
      ApiFactory.createApi(ApiTypes.AUTHENTICATION) as AuthService
    ).signUp(data.email, data.password, data.name);

    if (signUpResponse.Ok) {
      setLoading(false);
      dispatch(showToastify({ mode: "success", text: signUpResponse.data }));
      timer.current = setTimeout(() => {
        switchToSignIn();
      }, 2000);
    } else {
      setLoading(false);
    }
  };
  /*-------------------------------------------------------------- */
  return (
    <form className="w-96 p-8" onSubmit={handleSubmit(onSubmit)} noValidate>
      <Input
        error={errors?.name?.message}
        register={register("name", {
          required: "Name is required",
        })}
        type="text"
        label="Name"
        IconComponent={MdOutlineDriveFileRenameOutline}
        placeholder="Enter your name"
      />
      <Input
        error={errors?.email?.message}
        register={register("email", {
          required: "Email is required",
          pattern: {
            value: /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/,
            message: "Email is invalid",
          },
        })}
        type="email"
        label="Email"
        IconComponent={GoMail}
        placeholder="Enter your email"
      />
      <Input
        error={errors?.password?.message}
        register={register("password", {
          required: "Password is required",
        })}
        type="password"
        label="Password"
        IconComponent={RiLockPasswordLine}
        placeholder="Enter your password"
      />
      <div className="mt-10">
        <Button
          outline={false}
          handleClick={() => {}}
          name="sign up"
          type="submit"
          loading={loading}
        />
      </div>
      <p
        onClick={switchToSignIn}
        className="text-xs cursor-pointer text-gray-400 mt-2 text-center"
      >
        Sign In
      </p>
    </form>
  );
};

export default SignUp;
