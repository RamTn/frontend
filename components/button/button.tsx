import React from "react";
import { AiOutlineLoading3Quarters } from "react-icons/ai";

interface IButton {
  name: string;
  outline: boolean;
  handleClick: () => void;
  type?: "button" | "submit" | "reset" | undefined;
  loading?: boolean;
  disabled?: boolean;
}
const defaultStyle = `bg-teal-400 w-full h-9 text-white hover:bg-teal-600`;
const outlineStyle =
  "w-full h-9 text-teal-400 hover:bg-teal-400 hover:text-white border border-teal-400";

const Button: React.FC<IButton> = ({
  name,
  outline,
  handleClick,
  type,
  loading,
  disabled = false,
}) => {
  return (
    <button
      disabled={disabled}
      onClick={handleClick}
      className={`relative rounded-md ${disabled && "bg-gray-400"} ${
        outline ? outlineStyle : defaultStyle
      }`}
      type={type}
    >
      {!loading ? (
        name
      ) : (
        <div className="flex justify-center items-center">
          <AiOutlineLoading3Quarters className="animate-spin" />
        </div>
      )}
    </button>
  );
};

export default Button;
