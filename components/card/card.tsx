// libraries
import React, { ReactNode, useContext, useState } from "react";
import Image from "next/legacy/image";
import { AiOutlineHeart, AiFillHeart } from "react-icons/ai";
import dynamic from "next/dynamic";
import Link from "next/link";
import { useSession } from "next-auth/react";
import { useQuery } from "react-query";
// Apis
import { IProduct } from "@/domain/product/product";
import { ApiFactory, ApiTypes } from "@/application";
import FavouriteService from "@/application/favourite/favouriteService";
import { handleFavourite } from "@/features/favourite/favouriteSlice";
import CartService from "@/application/cart/cartService";
import RatingService from "@/application/rating/ratingService";
// Components
import Button from "../button/button";
import { useUtilities } from "@/hooks/useUtilities";
import { useAppDispatch } from "@/store/hooks";

const StarRatings = dynamic(() => import("react-star-ratings"), {
  ssr: false,
});

/* ---------------------- context configues -------------------- */
const cardContext = React.createContext<IProduct>({
  averageRating: 0,
  category: [],
  description: "",
  image: [],
  name: "",
  price: 0,
  subCategory: [],
  uuid: "",
  isLiked: false,
  isAddedToCart: false,
});

const useCardContext = () => {
  const context = useContext(cardContext);
  if (!context) {
    throw new Error(
      `Card compound components cannot be rendered outside the Card component`
    );
  }
  return context;
};

/* ------------------------ Card ------------------------ */
interface ICard {
  children: ReactNode;
  className?: string;
}

type ICardWithProduct = ICard & IProduct;

const cardContainerStyle =
  "my-10 xss:flex xss:flex-col xss:p-[15px]a flex flex-col items-start  md:flex-[0_0_48%] xl:flex-[0_0_32.5%] sm:shadow-lg rounded-md shadow-none sm:p-8 m-[5px]";

const Card = ({ children, className = "", ...props }: ICardWithProduct) => {
  const value = { ...props };
  return (
    <>
      <cardContext.Provider value={value}>
        <div className={`${className} ${cardContainerStyle}`}>{children}</div>
      </cardContext.Provider>
    </>
  );
};

/* ------------------------ Image ------------------------ */
const ImageCard: React.FC = () => {
  const { image, uuid, isLiked } = useCardContext();
  const dispatch = useAppDispatch();
  const [like, setLike] = useState(isLiked);
  const { data: session } = useSession();

  const clickOnLiked = async () => {
    if (session?.user?.access_token) {
      await (ApiFactory.createApi(ApiTypes.FAVOURITE) as FavouriteService)
        .addFavourite(uuid, session?.user?.uuid!)
        .then((res) => {
          if (res.Ok) {
            setLike(!like);
            dispatch(handleFavourite(uuid));
          }
        });
    }
  };

  return (
    <div className="relative rounded-md drop-shadow-lg self-center">
      <Link as={`/products/${uuid}`} href={`/products/${uuid}`}>
        <Image
          priority
          className="object-cover cursor-pointer object-center rounded-md"
          src={image[0]}
          blurDataURL={image[0] as string}
          alt="product-image"
          placeholder="blur"
          width={400}
          height={400}
        />
      </Link>
      <div className="absolute top-3 right-3">
        <div
          onClick={clickOnLiked}
          className="relative bg-white w-0 px-4 py-4 rounded-full flex items-center justify-center cursor-pointer"
        >
          {like ? (
            <AiFillHeart className="absolute" size={16} color="red" />
          ) : (
            <AiOutlineHeart className="absolute" size={16} color="red" />
          )}
        </div>
      </div>
    </div>
  );
};
/* ------------------------ title ------------------------ */
const Title: React.FC = () => {
  const { name, price } = useCardContext();
  const { addThreeDots } = useUtilities();

  return (
    <div className="flex justify-between w-full mt-2">
      <p className="text-gray-600 font-bold sm:text-md lg:text-lg">
        {`${addThreeDots(name, 20)}`}
      </p>
      <p className="text-gray-700 text-lg">{`${price} RM`}</p>
    </div>
  );
};
/* ------------------------ Description ------------------------ */

const Description: React.FC = () => {
  const { description } = useCardContext();
  const { addThreeDots } = useUtilities();
  return (
    <div className="w-90 my-2">
      <p className="text-gray-500 text-sm truncate ">
        {addThreeDots(description, 35)}
      </p>
    </div>
  );
};
/* ------------------------ StarRating ------------------------ */
const StarRating: React.FC = () => {
  const { data: session } = useSession();
  const { averageRating, uuid } = useCardContext();
  const [averageRatingState, setAverageRatingState] = useState(averageRating);
  const changeRating = (newRating: number) => {
    if (session?.user.access_token) {
      (ApiFactory.createApi(ApiTypes.RATING) as RatingService)
        .addRating({
          accountUuid: session?.user?.uuid!,
          productUuid: uuid,
          ratingValue: newRating,
        })
        .then((avrRating) => {
          setAverageRatingState(avrRating.data);
        });
    }
  };
  return (
    <div>
      <StarRatings
        rating={averageRatingState}
        starRatedColor="#2dd4bf"
        starHoverColor="#2dd4bf"
        changeRating={changeRating}
        numberOfStars={5}
        name="rating"
        starDimension="25px"
        starSpacing="2px"
      />
    </div>
  );
};
/* ------------------------ Button ------------------------ */
const ButtonCard: React.FC = () => {
  const { data: session } = useSession();
  const { uuid, price, isAddedToCart } = useCardContext();
  const [isAdded, setIsAddedToCart] = useState(isAddedToCart);

  const addCart = async () => {
    if (!isAdded) {
      (ApiFactory.createApi(ApiTypes.CART) as CartService).addCart({
        accountUuid: session?.user?.uuid!,
        productUuid: uuid,
        quantity: 1,
        totallPrice: price,
      });
    } else {
      (ApiFactory.createApi(ApiTypes.CART) as CartService).deleteCart({
        accountUuid: session?.user?.uuid!,
        productUuid: uuid,
      });
    }
  };
  const { refetch } = useQuery("cart", addCart, {
    enabled: false,
  });

  const handleAddToCart = () => {
    if (session?.user.access_token) {
      if (isAdded) setIsAddedToCart(false);
      else setIsAddedToCart(true);
      refetch();
    }
  };
  return (
    <div className="w-2/3 mt-8 mx-auto lg:w-1/3 lg:mx-0 lg:my-3">
      <Button
        handleClick={handleAddToCart}
        outline={!isAdded!}
        name={!isAdded ? `Add to cart` : "Added to cart"}
      />
    </div>
  );
};

Card.Image = ImageCard;
Card.Title = Title;
Card.StarRating = StarRating;
Card.Desrciption = Description;
Card.ButtonCard = ButtonCard;

export default Card;
