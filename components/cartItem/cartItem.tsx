// libraries
import React, { useState } from "react";
import Image from "next/legacy/image";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { AiOutlineCheckSquare } from "react-icons/ai";
// Apis
import { ApiFactory, ApiTypes } from "@/application";
import { ICart } from "@/domain/cart/cart";
import CartService from "@/application/cart/cartService";

interface CartItemProps {
  item: ICart;
  onToggle: (item: ICart) => void;
  deletedItem: (uuid: string) => void;
}

const CartItem: React.FC<CartItemProps> = ({ item, onToggle, deletedItem }) => {
  //-------------------------- states -------------------------//
  const [isChecked, setIsChecked] = useState(true);
  //----------------------- delete cart -----------------------//
  const handleCheck = () => {
    setIsChecked((value) => !value);
    onToggle(item);
  };
  const handleDeleteCart = async () => {
    await (ApiFactory.createApi(ApiTypes.CART) as CartService).deleteCartByUuid(
      item.uuid
    );
    deletedItem(item.uuid);
  };
  //-----------------------------------------------------------//
  return (
    <div className="flex items-center">
      <div className="flex justify-between  drop-shadow-lg bg-gray-50 items-center rounded-md mb-4 w-full">
        <div className="relative flex aspect-square w-32 flex-col items-center justify-center rounded-md">
          <Image
            src={item?.image[0]}
            priority
            alt="cart-image"
            className="rounded-lg"
            layout="fill"
          />
        </div>
        <h4 className="hidden sm:block w-56 truncate">{item.name}</h4>
        <h2>{item.totallPrice} $</h2>
        <h2>{item.quantity}</h2>
        <AiOutlineCloseCircle
          onClick={handleDeleteCart}
          size={25}
          className="mr-3 text-red-500 cursor-pointer"
        />
      </div>
      <AiOutlineCheckSquare
        onClick={handleCheck}
        className={`cursor-pointer ml-5 mb-4 ${
          isChecked ? "text-teal-400" : "text-gray-500"
        }`}
        size={30}
      />
    </div>
  );
};

export default CartItem;
