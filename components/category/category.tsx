import React from "react";
import Image from "next/legacy/image";
import { ICategory } from "@/domain/category/category";
import Link from "next/link";

interface ICategoryProps {
  categories: ICategory[];
}

const Category: React.FC<ICategoryProps> = ({ categories }) => {
  return (
    <div className="flex">
      {categories.map((category) => (
        <Link
          key={category.uuid}
          className="relative aspect-[3/5] lg:aspect-square w-full m-1 rounded-md"
          as={`/products?category[]=${category.name}`}
          href={`/products?category[]=${category.name}`}
        >
          <Image
            priority
            alt={category.name}
            src={category.image[0]}
            quality={80}
            layout="fill"
            className="object-cover object-center rounded-md"
          />
          <div className="absolute inset-0 cursor-pointer bg-black opacity-30 transition-opacity duration-300 hover:opacity-0 rounded-md"></div>
          <h1 className="absolute bottom-10 left-1/2 transform -translate-x-1/2 -translate-y-1/4 text-white text-xl md:text-4xl sm:text-3xl lg:text-10xl font-bold">
            {category.name}
          </h1>
        </Link>
      ))}
    </div>
  );
};

export default Category;
