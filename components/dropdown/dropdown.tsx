import React, { useImperativeHandle, useState, forwardRef } from "react";
import { HiFilter } from "react-icons/hi";
import { Transition } from "@headlessui/react";

interface IDropdown<T = () => void> {
  ref: React.Ref<unknown>;
  items: {
    handleClick: T;
    name: string;
    isClicked?: boolean;
  }[];
}

const dropdowncontainerStyle =
  "absolute right-0 w-48 mt-2 origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none";
const dowpdownItemStyle =
  "block w-full text-left px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900";

const Dropdown: React.FC<IDropdown> = forwardRef(({ items }, ref) => {
  //------------------------- states ------------------------//
  const [isOpen, setIsOpen] = useState(false);
  //------------------- toggling dropdown -------------------//
  const closeDropdown = () => setIsOpen(false);
  useImperativeHandle(ref, () => ({
    closeDropdown,
  }));

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };
  //--------------------------------------------------------//
  return (
    <div className="relative">
      <button
        className="w-full flex h-19 items-center px-4 py-2 text-sm text-white bg-teal-400 rounded-md"
        onClick={toggleDropdown}
      >
        <HiFilter className="mr-2" /> Filter
      </button>
      <Transition
        show={isOpen}
        enter="transition duration-100 ease-out"
        enterFrom="transform scale-95 opacity-0"
        enterTo="transform scale-100 opacity-100"
        leave="transition duration-75 ease-out"
        leaveFrom="transform scale-100 opacity-100"
        leaveTo="transform scale-95 opacity-0"
      >
        <div className={dropdowncontainerStyle}>
          <div className="py-1">
            {items.map((item) => (
              <button
                key={item.name}
                onClick={() => item.handleClick()}
                type="button"
                className={dowpdownItemStyle.concat(
                  " ",
                  `${item.isClicked ? "font-bold text-teal-400" : ""}`
                )}
              >
                {item.name}
              </button>
            ))}
          </div>
        </div>
      </Transition>
    </div>
  );
});

export default Dropdown;
