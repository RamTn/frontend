import React, { useEffect } from "react";
import { useAppDispatch } from "@/store/hooks";
import { showToastify } from "@/features/general/generalSlice";

interface IErrorProps {
  error: {
    statusCode: number;
    message: string;
    error: string;
  }[];
}

const ErrorPageHandler: React.FC<IErrorProps> = ({ error = [] }) => {
  const clearedErrors = error.filter((e) => !!e);
  const errorMesssage = clearedErrors.map(
    (e) => `${e?.statusCode} : ${e?.message}`
  );
  const dispatch = useAppDispatch();
  useEffect(() => {
    if (clearedErrors.length) {
      dispatch(
        showToastify({
          mode: "error",
          text: errorMesssage.join(", "),
        })
      );
    }
  }, [error]);
  return <></>;
};

export default ErrorPageHandler;
