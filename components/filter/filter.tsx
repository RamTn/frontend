// libraries
import { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import { useQuery } from "react-query";
import { useSession } from "next-auth/react";
// Apis
import { ApiFactory, ApiTypes } from "@/application";
import ProductService from "@/application/product/productService";
import { IProduct } from "@/domain/product/product";
// Components
import Dropdown from "@/components/dropdown/dropdown";
import SearchProduct from "../searchProduct/searchProduct";
import { useUtilities } from "@/hooks/useUtilities";
import Loading from "../loading/loading";

enum TCategory {
  MEN = "MEN",
  WOMEN = "WOMEN",
  KIDS = "KIDS",
}

enum TSubCategory {
  ALL = "ALL",
  CLOTHING = "CLOTHING",
  SHOES = "SHOES",
  GEAR = "GEAR",
}

interface IFilter {
  getClientProduct: (value: IProduct[]) => void;
}

const Filter: React.FC<IFilter> = ({ getClientProduct }) => {
  const [productQuery, setProductQuery] = useState<any>("");
  const { data: session } = useSession();
  const getProducts = async ({ queryKey: [_key, productQuery] }: any) => {
    return (
      ApiFactory.createApi(ApiTypes.PRODUCT) as ProductService
    ).getAllProducts({ ...productQuery, accountUuid: session?.user?.uuid });
  };
  const { data, refetch, isLoading } = useQuery(
    ["product", productQuery],
    getProducts,
    {
      enabled: false,
    }
  );
  /*----------------------------- states & Ref ----------------------------- */
  const dropdownRef = useRef<any>();

  const router = useRouter();
  const [query, setQuery] = useState<any>({
    category: "",
    subCategory: [TSubCategory.ALL],
  });
  const { extractQueryWithUrl } = useUtilities();
  /*------------------------------ lifecycle ------------------------------- */
  useEffect(() => {
    setQuery({
      ...query,
      category: [router.query["category[]"]],
      subCategory: router.query["subCategory[]"]
        ? [router.query["subCategory[]"]]
        : [TSubCategory.ALL],
    });
  }, []);

  useEffect(() => {
    if (data?.data) {
      getClientProduct(data?.data! || []);
    }
  }, [data]);

  useEffect(() => {
    if (productQuery) {
      refetch();
    }
  }, [productQuery]);

  useEffect(() => {
    // on browser back logic
    router.beforePopState(({ as }) => {
      if (as.substring(1, 8) === "product") {
        if (as !== router.asPath) {
          const prevQuery = as.match(/=(.*)/);
          if (prevQuery) {
            const urlQuery = extractQueryWithUrl(as);
            setQuery({
              ...query,
              category: urlQuery["category[]"]
                ? urlQuery["category[]"]
                : query.category,
              subCategory: urlQuery["subCategory[]"]
                ? urlQuery["subCategory[]"]
                : [TSubCategory.ALL],
            });

            setProductQuery(urlQuery);
          }
        }
      }
      return true;
    });

    return () => {
      router.beforePopState(() => true);
    };
  }, [router]);
  /*------------------------- filter category part ------------------------- */
  const renderFilterCategoryStyles = `text-xl mx-4  hover:text-teal-400 text-gray-600 `;
  const handleCategory = async (category: TCategory) => {
    const newQuery = {
      ...router.query,
      "category[]": category,
    };
    router.push({ pathname: "/products", query: newQuery }, undefined, {
      shallow: true,
    });
    setProductQuery(newQuery);
    setQuery({
      ...query,
      category: [category],
    });
  };

  const dropdown = () => (
    <div className=" w-30 !z-50 lg:hidden">
      <Dropdown items={dropDownItems} ref={dropdownRef} />
    </div>
  );

  const renderFilterCategory = () => (
    <div className="flex justify-around lg:justify-center mb-5 w-full lg:w-1/3 lg:m-0">
      <button
        onClick={() => handleCategory(TCategory.MEN)}
        className={renderFilterCategoryStyles.concat(
          " ",
          `${
            query.category.includes(TCategory.MEN)
              ? "font-bold text-teal-400"
              : ""
          }`
        )}
      >
        Men
      </button>
      <button
        onClick={() => handleCategory(TCategory.WOMEN)}
        className={renderFilterCategoryStyles.concat(
          " ",
          `${
            query.category.includes(TCategory.WOMEN)
              ? "font-bold text-teal-400"
              : ""
          }`
        )}
      >
        Women
      </button>
      <button
        onClick={() => handleCategory(TCategory.KIDS)}
        className={renderFilterCategoryStyles.concat(
          " ",
          `${
            query.category.includes(TCategory.KIDS)
              ? "font-bold text-teal-400"
              : ""
          }`
        )}
      >
        Kids
      </button>
      {dropdown()}
    </div>
  );
  /*------------------------ filter SubCategory part ----------------------- */
  const renderFilteSubCategoryStyles =
    "text-lg mx-4 hover:text-teal-400 text-gray-600";

  const handleSubCategory = async (subCategory: TSubCategory) => {
    let newQuery: any;
    if (subCategory !== TSubCategory.ALL) {
      newQuery = {
        ...router.query,
        "subCategory[]": [subCategory],
      };
    } else {
      delete router.query["subCategory[]"];
      newQuery = router.query;
    }
    router.push({ pathname: "/products", query: newQuery }, undefined, {
      shallow: true,
    });
    setQuery({
      ...query,
      subCategory: [subCategory],
    });
    setProductQuery(newQuery);
    if (dropdownRef?.current) {
      dropdownRef?.current?.closeDropdown();
    }
  };

  const renderFilterSubCategory = () => (
    <div className="hidden  justify-center lg:block w-full lg:w-1/3">
      <button
        onClick={() => handleSubCategory(TSubCategory.ALL)}
        className={renderFilteSubCategoryStyles.concat(
          " ",
          `${
            query.subCategory.includes(TSubCategory.ALL)
              ? "font-bold text-teal-400"
              : ""
          }`
        )}
      >
        All
      </button>
      <button
        onClick={() => handleSubCategory(TSubCategory.CLOTHING)}
        className={renderFilteSubCategoryStyles.concat(
          " ",
          `${
            query.subCategory.includes(TSubCategory.CLOTHING)
              ? "font-bold text-teal-400"
              : ""
          }`
        )}
      >
        Clothing
      </button>
      <button
        onClick={() => handleSubCategory(TSubCategory.SHOES)}
        className={renderFilteSubCategoryStyles.concat(
          " ",
          `${
            query.subCategory.includes(TSubCategory.SHOES)
              ? "font-bold text-teal-400"
              : ""
          }`
        )}
      >
        Shoes
      </button>
      <button
        onClick={() => handleSubCategory(TSubCategory.GEAR)}
        className={renderFilteSubCategoryStyles.concat(
          " ",
          `${
            query.subCategory.includes(TSubCategory.GEAR)
              ? "font-bold text-teal-400"
              : ""
          }`
        )}
      >
        Gear
      </button>
    </div>
  );
  /*----------------------------- dropdown part ----------------------------- */
  const dropDownItems = [
    {
      name: "All",
      handleClick: () => handleSubCategory(TSubCategory.ALL),
      isClicked: query.subCategory.includes(TSubCategory.ALL),
    },
    {
      name: "Clothing",
      handleClick: () => handleSubCategory(TSubCategory.CLOTHING),
      isClicked: query.subCategory.includes(TSubCategory.CLOTHING),
    },
    {
      name: "Shoes",
      handleClick: () => handleSubCategory(TSubCategory.SHOES),
      isClicked: query.subCategory.includes(TSubCategory.SHOES),
    },
    {
      name: "Gear",
      handleClick: () => handleSubCategory(TSubCategory.GEAR),
      isClicked: query.subCategory.includes(TSubCategory.GEAR),
    },
  ];

  /*----------------------------------------------------------------------- */
  return (
    <div className="flex flex-col justify-center items-center lg:flex-row">
      {renderFilterCategory()}
      {renderFilterSubCategory()}
      <div className="w-5/6 lg:w-1/3">
        {isLoading ? <Loading /> : <SearchProduct />}
      </div>
    </div>
  );
};

export default Filter;
