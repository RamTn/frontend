import React, { forwardRef } from "react";
import { IconType } from "react-icons";

interface IInputProps {
  IconComponent?: IconType;
  placeholder?: string;
  label?: string;
  type?: string;
  register?: any;
  error?: string;
}

const Input: React.ForwardRefRenderFunction<HTMLInputElement, IInputProps> = (
  { IconComponent, label, type, register, error, ...props },
  ref
) => {
  return (
    <div>
      {label && (
        <label className="text-gray-600" htmlFor={label}>
          {label}
        </label>
      )}
      <div className={`relative flex flex-col w-full ${label && "my-4"}`}>
        {error && (
          <p className="absolute bg-white py-2 px-1 bottom-[17px] left-[10px] text-red-500 text-xs mb-2">
            {error}
          </p>
        )}
        <input
          {...props}
          type={type}
          id={label || ""}
          ref={ref}
          {...register}
          className={`py-2 pl-10 pr-4 text-gray-500 border text-sm ${
            error ? "border-red-500" : "border-gray-300"
          } rounded-md focus:outline-none w-full`}
        />
        {IconComponent && (
          <div className="absolute top-3 left-3">
            <IconComponent className="w-5 h-4 text-gray-400 animate-pulse" />
          </div>
        )}
      </div>
    </div>
  );
};

const ForwardedInput = forwardRef(Input);

export default ForwardedInput;
