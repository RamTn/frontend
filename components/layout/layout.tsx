import { ReactNode } from "react";
import TabBar from "../tabBar/tabBar";

interface LayoutProps {
  children: ReactNode;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <div>
      <TabBar />
      <div className="container mx-auto pt-8 px-4">{children}</div>
    </div>
  );
};

export default Layout;
