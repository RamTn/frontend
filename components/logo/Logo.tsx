import React from "react";
import Image from "next/legacy/image";
import logo from "@/public/logo.png";

const Logo = () => {
  return (
    <div className="flex items-center ">
      <Image priority width={70} height={70} src={logo} alt="logo" />
      <div className="pl-4">
        <h3 className="text-teal-400 font-bold">GOSHOP</h3>
        <h6 className="text-teal-400">Let's Go Shopping</h6>
      </div>
    </div>
  );
};

export default Logo;
