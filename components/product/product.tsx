// libraries
import React, { ReactNode, useContext, useState } from "react";
import Image from "next/legacy/image";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";
import { useSession } from "next-auth/react";
import { useQuery } from "react-query";
// Apis
import { IProduct } from "@/domain/product/product";
import { addToCartList } from "@/features/cart/cartSlice";
import { ApiFactory, ApiTypes } from "@/application";
import CartService from "@/application/cart/cartService";
import RatingService from "@/application/rating/ratingService";
// Components
import Button from "../button/button";
import { useAppDispatch } from "@/store/hooks";

const StarRatings = dynamic(() => import("react-star-ratings"), {
  ssr: false,
});

/* ---------------------- context configues -------------------- */

interface IProductWithQuantity extends IProduct {
  setQuantity: React.Dispatch<React.SetStateAction<number>>;
  setTotall: React.Dispatch<React.SetStateAction<number>>;
  total: number;
  quantityState: number;
}

const productDetailContext = React.createContext<IProductWithQuantity>({
  averageRating: 0,
  category: [],
  description: "",
  image: [],
  name: "",
  price: 0,
  subCategory: [],
  uuid: "",
  setQuantity: () => {},
  setTotall: () => {},
  total: 1,
  quantityState: 1,
});

const useProductDetailContext = () => {
  const context = useContext(productDetailContext);
  if (!context) {
    throw new Error(
      `Product compound components cannot be rendered outside the Card component`
    );
  }
  return context;
};

/* ------------------------ Card ------------------------ */
interface IProductProps {
  children: ReactNode;
}

type TProductDetail = IProductProps & IProduct;

const ProductDetail = ({ children, ...props }: TProductDetail) => {
  const [quantityState, setQuantity] = useState(1);
  const [total, setTotall] = useState(Number(props.totallPrice || props.price));
  const value = { ...props, quantityState, setQuantity, total, setTotall };

  return (
    <productDetailContext.Provider value={value}>
      <div className="flex-col items-start lg:flex lg:flex-row ">
        {children}
      </div>
    </productDetailContext.Provider>
  );
};

/* ------------------------ path ------------------------ */
const DetailPath: React.FC = () => {
  const { category, subCategory } = useProductDetailContext();
  return (
    <div className="flex justify-between mt-10">
      <p>Catgeory :</p>
      {[...category, ...subCategory].join("/")}
    </div>
  );
};

/* ------------------------ Image ------------------------ */
const ImageDetail: React.FC = () => {
  const { image } = useProductDetailContext();

  return (
    <div className="relative w-full aspect-auto lg:w-2/3 drop-shadow-lg">
      <div className="relative aspect-square rounded-md">
        <Image
          className="object-center rounded-md"
          src={image?.[0]}
          blurDataURL={image?.[0] as string}
          alt="product-image"
          placeholder="blur"
          layout="fill"
        />
      </div>
    </div>
  );
};
/* ------------------------ Detail ------------------------ */
const Detail: React.FC<IProductProps> = ({ children }) => {
  return (
    <div className=" flex-col  lg:ml-10 text-gray-600 lg:w-1/3 h-2/3">
      {children}
    </div>
  );
};
/* ------------------------ title ------------------------ */
const DetailTitle: React.FC = () => {
  const { name } = useProductDetailContext();

  return <div className="mt-10 text-2xl font-bold lg:mt-0">{name}</div>;
};
/* ------------------------ Price ------------------------ */
const DetailPrice: React.FC = () => {
  const { price } = useProductDetailContext();

  return (
    <div className="flex justify-between mt-10">
      <div className="text-lg text-gray-500 font-bold">Price:</div>
      <div className="text-lg font-bold text-teal-400">{`${price} RM`}</div>
    </div>
  );
};
/* ------------------------ Description ------------------------ */
const DetailDescription: React.FC = () => {
  const { description } = useProductDetailContext();

  return <div className="mt-5 text-md ">{description}</div>;
};
/* ------------------------ StarRating ------------------------ */
const DetailStarRating: React.FC = () => {
  const { averageRating, uuid } = useProductDetailContext();
  const [averageRatingState, setAverageRatingState] = useState(averageRating);

  const { data: session } = useSession();
  const changeRating = (newRating: number) => {
    if (session?.user.access_token) {
      (ApiFactory.createApi(ApiTypes.RATING) as RatingService)
        .addRating({
          accountUuid: session?.user?.uuid!,
          productUuid: uuid,
          ratingValue: newRating,
        })
        .then((avrRating) => {
          setAverageRatingState(avrRating.data);
        });
    }
  };
  return (
    <div className="flex justify-between mt-10">
      <div>Rating:</div>
      <StarRatings
        rating={averageRatingState}
        starRatedColor="#2dd4bf"
        starHoverColor="#2dd4bf"
        changeRating={changeRating}
        numberOfStars={5}
        name="rating"
        starDimension="25px"
        starSpacing="2px"
      />
    </div>
  );
};
/* ------------------------ Quantity ------------------------ */
enum Quantity {
  INCREMENT = "INCREMENT",
  DECREMENT = "DECREMENT",
}
const DetailQuantity: React.FC = () => {
  const { price, totallPrice, quantityState, setQuantity, total, setTotall } =
    useProductDetailContext();

  const handleQuantity = (type: Quantity) => {
    if (type === Quantity.INCREMENT) {
      setQuantity((quantityState: number) => quantityState + 1);
      setTotall((totall) => totall + Number(price));
    }
    if (type === Quantity.DECREMENT) {
      if (quantityState !== 1) {
        setQuantity((quantityState: number) => quantityState - 1);
        setTotall((totall) => totall - Number(price));
      }
    }
  };

  return (
    <div className="flex justify-between mt-10 w-full">
      <div className="flex justify-center  items-center">
        <button
          onClick={() => handleQuantity(Quantity.DECREMENT)}
          className="bg-teal-400 w-10 h-8 rounded-tl-md rounded-bl-md text-white"
        >
          -
        </button>
        <div className="bg-teal-400 w-10 h-8 text-white text-center pt-1">
          {quantityState}
        </div>
        <button
          onClick={() => handleQuantity(Quantity.INCREMENT)}
          className="bg-teal-400 w-10 h-8 rounded-tr-md rounded-br-md text-white"
        >
          +
        </button>
      </div>
      <h2>{`totall: ${total.toFixed(2)} RM`}</h2>
    </div>
  );
};
/* ------------------------ Actions ------------------------ */
const DetailActions: React.FC = () => {
  const { data: session } = useSession();
  const dispatch = useAppDispatch();
  const router = useRouter();
  const { uuid, quantityState, total, image, name } = useProductDetailContext();

  const [isAdded, setIsAddedToCart] = useState(false);

  const addCart = async () => {
    if (!isAdded) {
      (ApiFactory.createApi(ApiTypes.CART) as CartService).addCart({
        accountUuid: session?.user?.uuid!,
        productUuid: uuid,
        quantity: quantityState,
        totallPrice: total,
      });
    } else {
      (ApiFactory.createApi(ApiTypes.CART) as CartService).deleteCart({
        accountUuid: session?.user?.uuid!,
        productUuid: uuid,
      });
    }
  };
  const { refetch } = useQuery("cart", addCart, {
    enabled: false,
  });

  const handleAddToCart = () => {
    if (session?.user.access_token) {
      if (isAdded) setIsAddedToCart(false);
      else setIsAddedToCart(true);
      refetch();
    }
  };
  const handleToBuy = () => {
    if (session?.user.access_token) {
      dispatch(
        addToCartList([
          {
            image: image,
            name,
            quantity: quantityState,
            totallPrice: total,
            uuid,
          },
        ])
      );
      router.push({ pathname: "/payment", query: { name: "product" } });
    }
  };
  return (
    <div className="flex justify-between mt-20">
      <div className="w-2/5">
        <Button
          outline={isAdded ? false : true}
          handleClick={handleAddToCart}
          name={!isAdded ? `Add to cart` : "Added to cart"}
        />
      </div>
      <div className="w-2/5">
        <Button outline={true} handleClick={handleToBuy} name="Buy now" />
      </div>
    </div>
  );
};

ProductDetail.DetailPath = DetailPath;
ProductDetail.Image = ImageDetail;
ProductDetail.Detail = Detail;
ProductDetail.DetailTitle = DetailTitle;
ProductDetail.DetailPrice = DetailPrice;
ProductDetail.DetailDescription = DetailDescription;
ProductDetail.DetailStarRating = DetailStarRating;
ProductDetail.DetailQuantity = DetailQuantity;
ProductDetail.DetailActions = DetailActions;

export default ProductDetail;
