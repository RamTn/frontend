// libraries
import React, { useEffect, useRef, useState } from "react";
import { FiSearch } from "react-icons/fi";
import { useRouter } from "next/router";
import debounce from "lodash.debounce";
// Apis
import { ApiFactory, ApiTypes } from "@/application";
import ProductService from "@/application/product/productService";
import { IProduct } from "@/domain/product/product";

interface ISerachProduct {
  getClientProduct?: (value: IProduct[]) => void;
}

const SearchProduct: React.FC<ISerachProduct> = ({ getClientProduct }) => {
  /*----------------------------- states & Router ----------------------------- */
  const router = useRouter();
  const inputRef = useRef<any>();
  const [inputChange, setInputChange] = useState("");
  const [searchProducts, setSearchProducts] = useState<IProduct[]>([]);
  /*-------------------------------- life cycle ------------------------------- */
  useEffect(() => {
    router.beforePopState(({ as }) => {
      if (as.substring(1, 7) === "search") {
        if (as !== router.asPath) {
          const prevQuery = as.match(/=(.*)/);
          if (prevQuery) {
            const query = {
              name: prevQuery[1],
            };

            getProducts(query).then((product) => {
              if (getClientProduct) {
                getClientProduct(product);
              }
              if (inputRef?.current) {
                inputRef.current.value = prevQuery[1];
              }
            });
          }
        }
      }
      return true;
    });

    return () => {
      router.beforePopState(() => true);
    };
  }, [router]);
  /*--------------- handle Input Change and make it throttled ----------------- */
  const handleInputChange = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const inputValue = event.target.value;
    setInputChange(inputValue);
    if (inputValue.length > 2) {
      const newQuery = {
        ...router.query,
        name: inputValue,
      };
      const productsResponse = await (
        ApiFactory.createApi(ApiTypes.PRODUCT) as ProductService
      ).searchProductsByName(newQuery);
      setSearchProducts(productsResponse.data);
    } else if (!inputValue) {
      setSearchProducts([]);
    }
  };

  const throttledHandleInputChange = debounce(handleInputChange, 700);
  /*----------------------------- get products ----------------------------- */
  const getProducts = async (queryUrl: any) => {
    const productsResponse = await (
      ApiFactory.createApi(ApiTypes.PRODUCT) as ProductService
    ).searchProductsByName(queryUrl);
    return productsResponse.data;
  };
  /*----------------------------- handle Submit ------------------------------ */
  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    const query = {
      name: inputChange,
    };
    router.push({ pathname: "/search", query }).then(async () => {
      const products = await getProducts(query);
      if (getClientProduct) getClientProduct(products);
      setSearchProducts([]);
    });
  };
  const handleRedirectToProductDetail = (uuid: string) => {
    router.push(`/products/${uuid}`);
  };
  /*-------------------------------------------------------------------------- */
  return (
    <form onSubmit={handleSubmit} className="relative flex w-full">
      <input
        ref={inputRef}
        type="text"
        onChange={throttledHandleInputChange}
        placeholder="Search"
        id="searchInput"
        className="py-2 pl-10 pr-4 text-gray-500 border border-gray-300 rounded-md focus:outline-none w-full"
      />
      <div className="absolute top-3 left-3">
        <FiSearch className="w-5 h-4 text-gray-400 animate-pulse" />
      </div>
      {searchProducts.length > 0 && (
        <ul className="absolute z-10 text-gray-500 bg-white top-10 mt-1 py-2 px-4 border w-full border-gray-300 rounded-md">
          {searchProducts.map(({ uuid, name }) => (
            <li
              onClick={() => handleRedirectToProductDetail(uuid)}
              className="py-2 cursor-pointer hover:text-teal-400"
              key={uuid}
            >
              {name}
            </li>
          ))}
        </ul>
      )}
    </form>
  );
};

export default SearchProduct;
