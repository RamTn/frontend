import { FC } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { IconType } from "react-icons";

interface ILargeLinkTabProps {
  IconComponent: IconType;
  name: string;
  url: string;
}

const LargeLinkTab: FC<ILargeLinkTabProps> = ({ IconComponent, name, url }) => {
  const router = useRouter();
  return (
    <div
      className={`${
        url === router.pathname && "border-b-2 border-teal-400"
      } flex flex-row-reverse  cursor-pointer justify-end items-center  m-2`}
    >
      <Link className="underlined" href={url}>
        <div
          className={`my-2 py-2 pl-3 pr-6 font-medium cursor-pointer md:text-sm lg:text-base ${
            url === router.pathname
              ? "text-teal-400 "
              : "text-gray-500 hover:text-gray-500 "
          }`}
        >
          {name}
        </div>
      </Link>
      <IconComponent
        className="hidden md:block"
        color={`${url === router.pathname ? "#2dd4bf" : "gray"}`}
        size={20}
      />
    </div>
  );
};

export default LargeLinkTab;
