// libraries
import { FC } from "react";
import { HiHome, HiPhone } from "react-icons/hi";
import { AiOutlinePoweroff, AiOutlineShoppingCart } from "react-icons/ai";
import { useSession } from "next-auth/react";
import { signIn, signOut } from "next-auth/react";
import { BsFillBagHeartFill } from "react-icons/bs";
// Components
import LargeLinkTabBar from "./largeLinkTab";
import Logo from "../logo/Logo";

const LargeScreenTabBar: FC = () => {
  const { data: session } = useSession();
  /*----------------------- handle sign in and sign out ------------------- */
  const handleAuthentication = () => {
    if (session?.user?.name) signOut();
    else signIn();
  };
  /*---------------------------- account render ---------------------------- */
  const accountRender = () => {
    return (
      <div className="flex w-1/5 justify-evenly md:mr-3">
        {session?.user.name && (
          <LargeLinkTabBar
            url={"/cart"}
            name={"Cart"}
            IconComponent={AiOutlineShoppingCart}
          />
        )}
        <div
          onClick={handleAuthentication}
          className="flex items-center cursor-pointer"
        >
          <AiOutlinePoweroff color="#2dd4bf" size={20} />
          <p className="text-gray-500 pl-2 cursor-pointer">
            {session?.user?.name ? "Sign Out" : "Sign in"}
          </p>
        </div>
      </div>
    );
  };
  /*------------------------------------------------------------------------- */
  return (
    <div className="hidden lg:flex h-20 items-center bg-gray-100 py-4">
      <div className="flex w-4/5 ml-9">
        <div className="mr-20">
          <Logo />
        </div>
        <LargeLinkTabBar url={"/"} name={"Home"} IconComponent={HiHome} />
        <LargeLinkTabBar
          url={"/about-us"}
          name={"About us"}
          IconComponent={HiPhone}
        />
        {session?.user.name && (
          <>
            <LargeLinkTabBar
              url={"/favourite"}
              name={"Favourite"}
              IconComponent={BsFillBagHeartFill}
            />
          </>
        )}
      </div>
      {accountRender()}
    </div>
  );
};

export default LargeScreenTabBar;
