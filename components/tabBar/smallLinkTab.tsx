import { FC } from "react";
import Link from "next/link";
import { IconType } from "react-icons/lib";
import { useRouter } from "next/router";

interface ISmallLinkTabProps {
  IconComponent: IconType;
  setIsMenuOpen: (value: boolean) => void;
  name: string;
  url: string;
}

const SmallLinkTab: FC<ISmallLinkTabProps> = ({
  setIsMenuOpen,
  IconComponent,
  name,
  url,
}) => {
  const router = useRouter();
  const handleOpenMenu = () => setIsMenuOpen(false);
  return (
    <div
      className={`${
        url === router.pathname && "bg-teal-400"
      } flex flex-row-reverse rounded-md cursor-pointer justify-end items-center w-5/6 my-2 pl-4`}
    >
      <Link className="underlined" href={url}>
        <div
          className={`my-2 py-2 px-8 font-medium cursor-pointer ${
            url === router.pathname ? "text-white " : "text-gray-500"
          }`}
          onClick={handleOpenMenu}
        >
          {name}
        </div>
      </Link>
      <IconComponent
        color={`${url === router.pathname ? "white" : "gray"}`}
        size={20}
      />
    </div>
  );
};

export default SmallLinkTab;
