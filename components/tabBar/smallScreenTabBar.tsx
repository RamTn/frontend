// libraries
import { useState } from "react";
import Image from "next/legacy/image";
import { HiPhone, HiHome, HiMenu } from "react-icons/hi";
import { signIn, signOut, useSession } from "next-auth/react";
import { AiOutlinePoweroff, AiOutlineShoppingCart } from "react-icons/ai";
import { BsFillBagHeartFill } from "react-icons/bs";
// Components
import Logo from "../logo/Logo";
import SmallLinkTab from "./smallLinkTab";
import logo from "@/public/logo.png";

const SmallScreenTabBar: React.FC = (): JSX.Element => {
  /*---------------------------------- states ----------------------------- */
  const { data: session } = useSession();
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  /*----------------------- handle sign in and sign out ------------------- */
  const profileRender = (): JSX.Element => (
    <>
      <div className="flex">
        <div className="rounded-full align-middle pl-4 mt-2">
          <Image
            src={logo}
            width={50}
            height={50}
            alt="profile"
            className="rounded-full"
          />
        </div>
      </div>
      <p className="ml-4 mt-4 font-bold">{session?.user.name}</p>
      <p className="ml-4 text-gray-500 text-sm">{session?.user.email}</p>
      <div className="border-b border-gray-300 mt-4" />
    </>
  );
  const handleAuthentication = () => {
    if (session?.user?.name) signOut();
    else signIn();
  };
  /*--------------------- Small link tab bars render ------------------- */
  const renderLinkTabBar = () => (
    <div className="h-full flex-col">
      <div className="flex flex-col items-center py-4">
        <SmallLinkTab
          IconComponent={HiHome}
          name="Home"
          setIsMenuOpen={setIsMenuOpen}
          url={"/"}
        />
        <SmallLinkTab
          IconComponent={HiPhone}
          name="About us"
          setIsMenuOpen={setIsMenuOpen}
          url={"/about-us"}
        />
        {session?.user.name && (
          <>
            <SmallLinkTab
              url={"/favourite"}
              setIsMenuOpen={setIsMenuOpen}
              name={"Favourite"}
              IconComponent={BsFillBagHeartFill}
            />

            <SmallLinkTab
              IconComponent={AiOutlineShoppingCart}
              name="Cart"
              setIsMenuOpen={setIsMenuOpen}
              url={"/cart"}
            />
          </>
        )}
      </div>
      <div
        onClick={handleAuthentication}
        className="flex w-full border-t border-gray-300  pl-7 items-center py-3 absolute bottom-0 left-0"
      >
        <AiOutlinePoweroff color="#2dd4bf" size={25} />
        <p className="pl-7 text-lg  text-gray-500 cursor-pointer">
          {session?.user?.name ? "Sign Out" : "Sign in"}
        </p>
      </div>
    </div>
  );
  /*--------------------------------------------------------------------- */
  const menuClass = `lg:hidden fixed top-0 left-0 h-full w-2/3 bg-gray-100 z-50 transform duration-300 ease-in-out ${
    isMenuOpen ? "translate-x-0" : "-translate-x-full"
  }`;
  return (
    <>
      <div className="lg:hidden flex w-auto justify-between h-20 px-10 items-center bg-gray-100 py-4">
        <HiMenu
          className="text-gray-600 cursor-pointer"
          size={24}
          onClick={() => setIsMenuOpen(true)}
        />
        <div className="hidden sm:block">
          <Logo />
        </div>
      </div>

      {isMenuOpen && (
        <div
          onClick={() => setIsMenuOpen(false)}
          className="absolute top-0 left-0 z-10 bg-black bg-opacity-50 w-full h-full"
        />
      )}
      <div className={menuClass}>
        {profileRender()}
        {renderLinkTabBar()}
      </div>
    </>
  );
};

export default SmallScreenTabBar;
