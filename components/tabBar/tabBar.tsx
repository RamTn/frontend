import SmallScreenTabBar from "./smallScreenTabBar";
import LargeScreenTabBar from "./largeScreenTabBar";

const TabBar = () => {
  return (
    <>
      <LargeScreenTabBar />
      <SmallScreenTabBar />
    </>
  );
};

export default TabBar;
