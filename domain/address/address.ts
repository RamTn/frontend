export interface IAddress {
  country: string;
  state: string;
  postalCode: number;
  phone: string;
  address: string;
  active: boolean;
  uuid: string;
}

export class Address {
  _country = "";
  _state = "";
  _postalCode = 0;
  _phone = "";
  _address = "";
  _active = false;
  _uuid = "";
  constructor({
    uuid,
    country,
    state,
    postalCode,
    phone,
    address,
    active,
  }: IAddress) {
    this._uuid = uuid;
    this._country = country;
    this._state = state;
    this._postalCode = postalCode;
    this._phone = phone;
    this._address = address;
    this._active = active;
  }
  get uuid() {
    return this._uuid;
  }

  set uuid(value) {
    this._uuid = value;
  }

  get country() {
    return this._country;
  }

  set country(value) {
    if (typeof value === "string") this._country = value;
    else this._country = "";
  }

  get active() {
    return this._active;
  }

  set active(value) {
    if (typeof value === "boolean") this._active = value;
    else this._active = false;
  }

  get state() {
    return this._state;
  }

  set state(value) {
    if (typeof value === "string") this._state = value;
    else this._state = "";
  }

  get postalCode() {
    return this._postalCode;
  }

  set postalCode(value) {
    if (typeof value === "number") this._postalCode = value;
    else this._postalCode = 0;
  }

  get phone() {
    return this._phone;
  }

  set phone(value) {
    if (typeof value === "string") this._phone = value;
    else this._phone = "";
  }

  get address() {
    return this._address;
  }

  set address(value) {
    if (typeof value === "string") this._address = value;
    else this._address = "";
  }

  getData() {
    return {
      country: this.country,
      state: this.state,
      postalCode: this.postalCode,
      phone: this.phone,
      address: this.address,
      active: this.active,
      uuid: this.uuid,
    };
  }
}
