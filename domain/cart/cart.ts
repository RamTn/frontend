import notFoundImage from "@/public/not-found-image.jpeg";
import { StaticImageData } from "next/image";

export interface ICart {
  uuid: string;
  totallPrice: number;
  quantity: number;
  name: string;
  image: string[] | StaticImageData[];
}

export class Cart {
  _uuid = "";
  _totallPrice = 0;
  _image: string[] | StaticImageData[] = [];
  _quantity = 0;
  _name = "";
  constructor({ uuid, name, image, quantity, totallPrice }: ICart) {
    this._uuid = uuid;
    this._name = name;
    this._image = image;
    this._quantity = quantity;
    this._totallPrice = totallPrice;
  }

  get uuid() {
    return this._uuid;
  }

  set uuid(value) {
    this._uuid = value;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    if (typeof value === "string") this._name = value;
    else this._name = "";
  }

  get image() {
    return this._image;
  }

  set image(value) {
    if (Array.isArray(value)) this._image = value;
    else this._image = [notFoundImage];
  }

  get totallPrice() {
    return this._totallPrice;
  }

  set totallPrice(value) {
    if (typeof value === "number") this._totallPrice = value;
    else this._totallPrice = 0;
  }

  get quantity() {
    return this._quantity;
  }

  set quantity(value) {
    if (typeof value === "number") this._quantity = value;
    else this._quantity = 0;
  }

  getData() {
    return {
      uuid: this.uuid,
      name: this.name,
      image: this.image,
      totallPrice: this.totallPrice,
      quantity: this.quantity,
    };
  }
}
