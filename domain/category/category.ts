import notFoundImage from "@/public/not-found-image.jpeg";
import { StaticImageData } from "next/image";

export interface ICategory {
  uuid: string;
  name: string;
  image: string[] | StaticImageData[];
}

export class Category {
  _uuid = "";
  _name = "";
  _image: string[] | StaticImageData[] = [];
  constructor({ uuid, name, image }: ICategory) {
    this._uuid = uuid;
    this._name = name;
    this._image = image;
  }

  get uuid() {
    return this._uuid;
  }

  set uuid(value) {
    this._uuid = value;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    if (typeof value === "string") this._name = value;
    else this._name = "";
  }

  get image() {
    return this._image;
  }

  set image(value) {
    if (Array.isArray(value)) this._image = value;
    else this._image = [notFoundImage];
  }

  getData() {
    return {
      uuid: this.uuid,
      name: this.name,
      image: this.image,
    };
  }
}
