import { StaticImageData } from "next/image";
import notFoundImage from "@/public/not-found-image.jpeg";

export interface IProduct {
  uuid: string;
  name: string;
  price: number;
  description: string;
  image: string[] | StaticImageData[];
  category: string[];
  subCategory: string[];
  averageRating: number;
  isLiked?: boolean;
  isAddedToCart?: boolean;
  quantity?: number;
  totallPrice?: number;
}

export class Product {
  _uuid = "";
  _name = "";
  _description = "";
  _price = 0;
  _averageRating = 0;
  _category: string[] = [];
  _image: string[] | StaticImageData[] = [];
  _subcategory: string[] = [];
  _isLiked?: boolean = false;
  _isAddedToCart?: boolean = false;
  _quantity?: number = 0;
  _totallPrice?: number = 0;
  constructor({
    uuid,
    name,
    description,
    price,
    averageRating,
    category,
    image,
    subCategory,
    isLiked,
    isAddedToCart,
    totallPrice,
    quantity,
  }: IProduct) {
    this.uuid = uuid;
    this.name = name;
    this.description = description;
    this.price = price;
    this.averageRating = averageRating;
    this.category = category;
    this.image = image;
    this.subcategory = subCategory;
    this.isLiked = isLiked;
    this.isAddedToCart = isAddedToCart;
    this.quantity = quantity;
    this.totallPrice = totallPrice;
  }

  get subcategory() {
    return this._subcategory;
  }

  set subcategory(value) {
    if (Array.isArray(value)) this._subcategory = value;
    else this._subcategory = [];
  }

  get isLiked() {
    return this._isLiked;
  }

  set isLiked(value) {
    if (typeof value === "boolean") this._isLiked = value;
    else this._isLiked = false;
  }

  get isAddedToCart() {
    return this._isAddedToCart;
  }

  set isAddedToCart(value) {
    if (typeof value === "boolean") this._isAddedToCart = value;
    else this._isAddedToCart = false;
  }

  get image() {
    return this._image;
  }

  set image(value) {
    if (Array.isArray(value)) this._image = value;
    else this._image = [notFoundImage];
  }

  get category() {
    return this._category;
  }

  set category(value) {
    if (Array.isArray(value)) this._category = value;
    else this._category = [];
  }

  get averageRating() {
    return this._averageRating;
  }

  set averageRating(value) {
    if (typeof value === "number") this._averageRating = value;
    else this._averageRating = 0;
  }

  get uuid() {
    return this._uuid;
  }

  set uuid(value) {
    this._uuid = value;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    if (typeof value === "string") this._name = value;
    else this._name = "";
  }

  get quantity() {
    return this._quantity;
  }

  set quantity(value) {
    if (typeof value === "number") this._quantity = value;
    else this._quantity = 0;
  }

  get totallPrice() {
    return this._totallPrice;
  }

  set totallPrice(value) {
    if (typeof value === "number") this._totallPrice = value;
    else this._totallPrice = 0;
  }

  get description() {
    return this._description;
  }

  set description(value) {
    if (typeof value === "string") this._description = value;
    else this._description = "";
  }

  get price() {
    return this._price;
  }

  set price(value) {
    if (typeof value === "number") this._price = value;
    else this._price = 0;
  }

  getData(): IProduct {
    return {
      uuid: this.uuid,
      name: this.name,
      price: this.price,
      description: this.description,
      averageRating: this.averageRating,
      category: this.category,
      image: this.image,
      subCategory: this.subcategory,
      isLiked: this._isLiked,
      isAddedToCart: this._isAddedToCart,
      totallPrice: this._totallPrice,
      quantity: this._quantity,
    };
  }
}
