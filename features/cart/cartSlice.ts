import { ICart } from "@/domain/cart/cart";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface ICartState {
  cartsList: ICart[];
}

const initialState: ICartState = {
  cartsList: [],
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addToCartList(state, action: PayloadAction<ICart[]>) {
      state.cartsList = action.payload;
    },
  },
});

export const { addToCartList } = cartSlice.actions;

export default cartSlice.reducer;
