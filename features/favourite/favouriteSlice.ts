import { IProduct } from "@/domain/product/product";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface IFavouriteState {
  favouritesList: IProduct[];
}

const initialState: IFavouriteState = {
  favouritesList: [],
};

const favouriteSlice = createSlice({
  name: "favourite",
  initialState,
  reducers: {
    getListFavourite(state, action: PayloadAction<IProduct[]>) {
      state.favouritesList = action.payload;
    },
    handleFavourite(state, action: PayloadAction<string>) {
      const foundProduct = state.favouritesList.find(
        (product) => product.uuid === action.payload
      );
      if (foundProduct) {
        state.favouritesList = state.favouritesList.filter(
          (prodcut) => prodcut.uuid !== action.payload
        );
      }
    },
  },
});

export const { handleFavourite, getListFavourite } = favouriteSlice.actions;

export default favouriteSlice.reducer;
