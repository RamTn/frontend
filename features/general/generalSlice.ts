import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { toast } from "react-toastify";

interface IGeneralState {
  activeTab: number;
  isMenuOpen: boolean;
  isAccountModalOpen: boolean;
}

const initialState: IGeneralState = {
  activeTab: 0,
  isMenuOpen: false,
  isAccountModalOpen: false,
};

const generalSlice = createSlice({
  name: "general",
  initialState,
  reducers: {
    setActiveTab(state, action: PayloadAction<number>) {
      state.activeTab = action.payload;
    },
    setIsMenuOpen(state, action: PayloadAction<boolean>) {
      state.isMenuOpen = action.payload;
    },
    setIsAccountModalOpen(state, action: PayloadAction<boolean>) {
      state.isAccountModalOpen = action.payload;
    },
    showToastify(state, action: PayloadAction<{ mode: string; text: string }>) {
      const { mode, text } = action.payload;

      if (mode === "success") {
        toast.success(text, {
          position: toast.POSITION.BOTTOM_RIGHT,
          className: "toastify",
        });
      }
      if (mode === "error") {
        toast.error(text, {
          position: toast.POSITION.BOTTOM_RIGHT,
          className: "toastify",
        });
      }
      if (mode === "warning") {
        toast.warn(text, {
          position: toast.POSITION.BOTTOM_RIGHT,
          className: "toastify",
        });
      }
    },
  },
});

export const {
  setActiveTab,
  setIsMenuOpen,
  showToastify,
  setIsAccountModalOpen,
} = generalSlice.actions;

export default generalSlice.reducer;
