export const useUtilities = () => {
  const extractQueryWithUrl = (query: string) => {
    const url = new URL(`${window.location.protocol}${query}`);
    const searchParams = new URLSearchParams(url.search) as any;

    const queryParams = {} as any;

    for (const [key, value] of searchParams) {
      if (!queryParams[key]) {
        queryParams[key] = [];
      }
      queryParams[key].push(value);
    }

    return queryParams;
  };
  const addThreeDots = (text: string, num: number) => {
    if (text.length > num) {
      return `${text.slice(0, num)} ...`;
    }
    return text;
  };
  return {
    extractQueryWithUrl,
    addThreeDots,
  };
};
