import { api } from "../";
import { Result } from "true-myth";
import { IAddress } from "@/domain/address/address";

interface IAddAddress {
  accountUuid: string;
  address: string;
  phone: string;
  postalCode: string;
  state: string;
  country: string;
}

class AddressRepository {
  private static instance: AddressRepository;
  private addAddressUrl = "/user/address/";
  private activateAddressUrl = "/user/address/activate/";
  private deleteAddressUrl = "/user/address/delete/";

  static getInstance(): AddressRepository {
    if (!AddressRepository.instance) {
      AddressRepository.instance = new AddressRepository();
    }
    return AddressRepository.instance;
  }

  async getAddresses(
    accountUuid: string
  ): Promise<Result<IAddress[], unknown>> {
    try {
      const response = await api.get(`${this.addAddressUrl}${accountUuid}`, {
        headers: { includeToken: true },
      });

      return Result.ok(response.data);
    } catch (error) {
      return Result.err(error);
    }
  }

  async deleteAddress(
    accountUuid: string,
    addressUuid: string
  ): Promise<Result<string, unknown>> {
    try {
      const response = await api.delete(
        `${this.deleteAddressUrl}${accountUuid}/${addressUuid}`,
        {
          headers: { includeToken: true },
        }
      );
      return Result.ok(response.data);
    } catch (error) {
      return Result.err(error);
    }
  }

  async activeAddress(
    accountUuid: string,
    addressUuid: string
  ): Promise<Result<string, unknown>> {
    try {
      const reponse = await api.get(
        `${this.activateAddressUrl}${accountUuid}/${addressUuid}`,
        {
          headers: { includeToken: true },
        }
      );
      const message = reponse.data;

      return Result.ok(message);
    } catch (error) {
      return Result.err(error);
    }
  }

  async addAddress({
    accountUuid,
    address,
    phone,
    postalCode,
    state,
    country,
  }: IAddAddress): Promise<Result<IAddress[], unknown>> {
    try {
      const response = await api.post(
        `${this.addAddressUrl}${accountUuid}`,
        {
          accountUuid,
          address,
          phone,
          postalCode,
          state,
          country,
        },
        { headers: { includeToken: true } }
      );

      return Result.ok(response.data);
    } catch (error) {
      return Result.err(error);
    }
  }
}

export default AddressRepository;
