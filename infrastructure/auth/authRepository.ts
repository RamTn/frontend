import { api } from "../";
import { Result } from "true-myth";

class AuthRepository {
  private static instance: AuthRepository;
  private authUrl = "auth";
  private signInUrl = `${this.authUrl}/signIn`;
  private signUpUrl = `${this.authUrl}/signUp`;

  static getInstance(): AuthRepository {
    if (!AuthRepository.instance) {
      AuthRepository.instance = new AuthRepository();
    }
    return AuthRepository.instance;
  }

  async signIn(
    email: string,
    password: string
  ): Promise<Result<string, unknown>> {
    try {
      const signInData = await api.post(this.signInUrl, { email, password });
      const token = signInData.data;

      return Result.ok(token);
    } catch (error) {
      return Result.err(error);
    }
  }

  async signUp(
    email: string,
    password: string,
    name: string
  ): Promise<Result<string, unknown>> {
    try {
      const response = await api.post(this.signUpUrl, {
        email,
        password,
        name,
      });

      return Result.ok(response.data);
    } catch (error) {
      return Result.err(error);
    }
  }
}

export default AuthRepository;
