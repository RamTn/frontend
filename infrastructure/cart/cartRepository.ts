import { api } from "../";
import { Result } from "true-myth";
import { ICart } from "@/domain/cart/cart";

interface IAddCart {
  accountUuid: string;
  productUuid: string;
  quantity: number;
  totallPrice: number;
}

interface IdeletedCart {
  accountUuid: string;
  productUuid: string;
}

class CartRepository {
  private static instance: CartRepository;
  private getCartsUrl = "cart";
  private getDeleteCartsUrl = "cart/delete-carts";

  static getInstance(): CartRepository {
    if (!CartRepository.instance) {
      CartRepository.instance = new CartRepository();
    }
    return CartRepository.instance;
  }

  async getCarts(accountUuid: string): Promise<Result<ICart[], unknown>> {
    try {
      const cartsData = await api.get(`${this.getCartsUrl}/${accountUuid}`, {
        headers: { includeToken: true },
      });
      const carts = cartsData.data;
      return Result.ok(carts);
    } catch (error) {
      return Result.err(error);
    }
  }

  async deleteCart({
    accountUuid,
    productUuid,
  }: IdeletedCart): Promise<Result<string, unknown>> {
    try {
      const cartsData = await api.delete(
        `${this.getCartsUrl}/${productUuid}/${accountUuid}`,
        {
          headers: { includeToken: true },
        }
      );
      const carts = cartsData.data;

      return Result.ok(carts);
    } catch (error) {
      return Result.err(error);
    }
  }

  async deleteCarts(deleteCarts: string[]): Promise<Result<string, unknown>> {
    try {
      const cartsData = await api.delete(
        `${this.getDeleteCartsUrl}?items=${deleteCarts}`,
        {
          headers: { includeToken: true },
        }
      );
      const carts = cartsData.data;

      return Result.ok(carts);
    } catch (error) {
      return Result.err(error);
    }
  }

  async deleteCartByUuid(uuid: string): Promise<Result<string, unknown>> {
    try {
      const cartsData = await api.delete(`${this.getCartsUrl}/${uuid}`, {
        headers: { includeToken: true },
      });
      const carts = cartsData.data;

      return Result.ok(carts);
    } catch (error) {
      return Result.err(error);
    }
  }

  async addCart({
    accountUuid,
    productUuid,
    quantity,
    totallPrice,
  }: IAddCart): Promise<Result<ICart[], unknown>> {
    try {
      const response = await api.post(
        `${this.getCartsUrl}`,
        {
          accountUuid,
          productUuid,
          quantity,
          totallPrice,
        },
        { headers: { includeToken: true } }
      );

      return Result.ok(response.data);
    } catch (error) {
      return Result.err(error);
    }
  }
}

export default CartRepository;
