import { ICategory } from "@/domain/category/category";
import { api } from "../";
import { Result } from "true-myth";

class CategoryRepository {
  private static instance: CategoryRepository;
  private getCategoriesUrl = "category";

  static getInstance(): CategoryRepository {
    if (!CategoryRepository.instance) {
      CategoryRepository.instance = new CategoryRepository();
    }
    return CategoryRepository.instance;
  }

  async getCategories(): Promise<Result<ICategory[], unknown>> {
    try {
      const categoriesData = await api(this.getCategoriesUrl);
      const categories = categoriesData.data;

      return Result.ok(categories);
    } catch (error) {
      return Result.err(error);
    }
  }
}

export default CategoryRepository;
