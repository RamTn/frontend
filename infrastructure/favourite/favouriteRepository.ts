import { api } from "../";
import { Result } from "true-myth";
import { IProduct } from "@/domain/product/product";

class FavouriteRepository {
  private static instance: FavouriteRepository;
  private getFavouriteUrl = "favourite";
  private getAccountFavouriteUrl = `${this.getFavouriteUrl}/account-favourite`;

  static getInstance(): FavouriteRepository {
    if (!FavouriteRepository.instance) {
      FavouriteRepository.instance = new FavouriteRepository();
    }
    return FavouriteRepository.instance;
  }

  async getAllFavouritesByAccountUuid(
    accountUuid: string,
    token: string
  ): Promise<Result<IProduct[], unknown>> {
    try {
      const favouriteData = await api(
        `${this.getAccountFavouriteUrl}/${accountUuid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
          method: "GET",
        }
      );
      const favouriteProducts = favouriteData.data;

      return Result.ok(favouriteProducts);
    } catch (error) {
      return Result.err(error);
    }
  }

  async addFavoutite(
    productUuid: string,
    accountUuid: string
  ): Promise<Result<string, unknown>> {
    try {
      const favouriteData = await api(this.getFavouriteUrl, {
        headers: { includeToken: true },
        method: "POST",
        data: {
          productUuid,
          accountUuid,
        },
      });
      const favourite = favouriteData.data;

      return Result.ok(favourite);
    } catch (error) {
      return Result.err(error);
    }
  }
}

export default FavouriteRepository;
