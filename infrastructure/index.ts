import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { getSession } from "next-auth/react";
import { signOut } from "next-auth/react";

export const baseURL =
  process.env.NODE_ENV === "production"
    ? process.env.NEXT_PUBLIC_API_URL
    : "http://localhost:4000";

// Create a function to configure the request headers
const configureHeaders = async (
  config: AxiosRequestConfig
): Promise<AxiosRequestConfig> => {
  const session = await getSession();
  const token = session?.user.access_token;

  // Check if the 'includeToken' header is provided
  const includeToken = config.headers!["includeToken"];

  // Add the token to the headers if 'includeToken' is true and token is available
  if (includeToken && token) {
    config.headers!["Authorization"] = `Bearer ${token}`;
  }

  return config;
};

// Create the axios instance with the configured headers
const api: AxiosInstance = axios.create({
  baseURL,
  headers: {},
});

// Apply the headers configuration interceptor
api.interceptors.request.use(configureHeaders as any, (error) => {
  if (error.response.status === 401) {
    signOut();
  }
});

export { api };
