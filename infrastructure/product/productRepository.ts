import { Product, IProduct } from "../../domain/product/product";
import { api } from "../index";
import { Result } from "true-myth";

type TQuery = NodeJS.Dict<string | string[]>;

class ProductRepository {
  private getProductsUrl = "product";
  private getSearchByNameUrl = `${this.getProductsUrl}/search`;
  private getPopularsUrl = `${this.getProductsUrl}/popular`;

  async getAllProducts(query: TQuery): Promise<Result<IProduct[], unknown>> {
    try {
      const url = this.createUrlPath(query, this.getProductsUrl);

      const productsData = await api(url);
      const products = productsData.data;

      return Result.ok(products);
    } catch (error) {
      return Result.err(error);
    }
  }

  async searchProductsByName(
    query: TQuery
  ): Promise<Result<IProduct[], unknown>> {
    try {
      const url = this.createUrlPath(query, this.getSearchByNameUrl);

      const productsData = await api(url);
      const products = productsData.data;

      return Result.ok(products);
    } catch (error) {
      return Result.err(error);
    }
  }

  createUrlPath(queryData: any, preQueryPath: string): string {
    let result = "";
    const keys = Object.keys(queryData);

    if (keys.length === 0) return "";

    keys.forEach((key) => {
      if (Array.isArray(queryData[key])) {
        queryData[key].forEach((value: any) => {
          result += `${key}=${encodeURIComponent(value)}&`;
        });
      } else {
        result += `${key}=${encodeURIComponent(queryData[key])}&`;
      }
    });

    result = result.slice(0, -1);

    return `${preQueryPath}?${result}`;
  }

  async getProductByUuid(productUuid: string, accountUuid?: string) {
    try {
      const productsData = await api(
        `${this.getProductsUrl}/${productUuid}/${accountUuid}`
      );
      const product = productsData.data;

      return Result.ok(product);
    } catch (error) {
      return Result.err(error);
    }
  }

  async getPropulars(rating: number, accountUuid?: string) {
    try {
      const productsData = await api(
        `${this.getPopularsUrl}/${rating}/${accountUuid}`
      );

      const product = productsData.data;

      return Result.ok(product);
    } catch (error) {
      return Result.err(error);
    }
  }

  async createProduct(product: Product) {
    // Add logic to create a new product on the backend API
  }

  async updateProduct(productId: string, product: Product) {
    // Add logic to update an existing product on the backend API
  }

  async deleteProduct(productId: string) {
    // Add logic to delete an existing product on the backend API
  }
}

export default ProductRepository;
