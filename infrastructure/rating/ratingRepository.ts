import { api } from "../";
import { Result } from "true-myth";

interface IAddRating {
  productUuid: string;
  accountUuid: string;
  ratingValue: number;
}

class RatingRepository {
  private static instance: RatingRepository;
  private addRatingUrl = "rating";

  static getInstance(): RatingRepository {
    if (!RatingRepository.instance) {
      RatingRepository.instance = new RatingRepository();
    }
    return RatingRepository.instance;
  }

  async addRating({
    accountUuid,
    productUuid,
    ratingValue,
  }: IAddRating): Promise<Result<number, unknown>> {
    try {
      const ratingData = await api.post(
        this.addRatingUrl,
        {
          userUuid: accountUuid,
          productUuid,
          ratingValue,
        },
        { headers: { includeToken: true } }
      );
      const averageRating = ratingData.data;

      return Result.ok(averageRating);
    } catch (error) {
      return Result.err(error);
    }
  }
}

export default RatingRepository;
