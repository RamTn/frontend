/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ["product-api.s3.ap-southeast-1.amazonaws.com"],
  },
  async headers() {
    return [
      {
        source: "/",
        headers: [
          {
            key: "Cache-Control",
            value: "public, max-age=1",
          },
        ],
      },
      {
        source: "/products",
        headers: [
          {
            key: "Cache-Control",
            value: "public, max-age=1",
          },
        ],
      },
      {
        source: "/search",
        headers: [
          {
            key: "Cache-Control",
            value: "public, max-age=1",
          },
        ],
      },
    ];
  },
  trailingSlash: true,
};

module.exports = nextConfig;
