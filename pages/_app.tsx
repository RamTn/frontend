// libraries
import type { AppProps } from "next/app";
import { Provider } from "react-redux";
import { SessionProvider } from "next-auth/react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { QueryClientProvider, QueryClient } from "react-query";
import "../styles/globals.css";
// Components
import { store } from "@/store";
import Layout from "@/components/layout/layout";
import ErrorPageHandler from "@/components/errorPageHandler/errorPageHandler";

const queryClient = new QueryClient();

interface IProps extends AppProps {
  session: any;
}

export default function App({ Component, pageProps, session }: IProps) {
  return (
    <QueryClientProvider client={queryClient}>
      <Provider store={store}>
        <SessionProvider session={session}>
          <Layout>
            <Component {...pageProps} />
            <ErrorPageHandler {...pageProps} />
            <ToastContainer
              theme="colored"
              pauseOnHover
              className="rounded-sm text-sm"
            />
          </Layout>
        </SessionProvider>
      </Provider>
    </QueryClientProvider>
  );
}
