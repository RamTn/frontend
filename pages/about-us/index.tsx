// libraries
import React from "react";
import Head from "next/head";
import Image from "next/legacy/image";
// Components
import Logo from "@/components/logo/Logo";
import instagram from "@/public/instagram.png";
import whatsapp from "@/public/whatsapp.png";
import linkedin from "@/public/linkedin.png";

const AboutUsPage = () => {
  //----------------------- handle social media -----------------------//
  const openInstagram = () => {
    window.open("https://www.instagram.com/ramtin_shj", "_blank");
  };

  const openWhatsApp = () => {
    window.open("https://wa.me/+601112975929", "_blank");
  };

  const openLinkedIn = () => {
    window.open(
      "https://www.linkedin.com/in/ramtin-sharif-jalaly-166567178/",
      "_blank"
    );
  };
  //------------------------------------------------------------------//
  return (
    <div className="container mx-auto">
      <Head>
        <title>About Us - Your Company Name</title>
        <meta
          name="description"
          content="Welcome to GOSHOP, your ultimate online shopping destination. Explore a wide range of products from various categories. Connect with us on Instagram, WhatsApp, and LinkedIn for the latest updates and exclusive deals."
        />
      </Head>

      <h1 className="text-2xl text-gray-500 font-bold mb-4">About Us</h1>
      <p className="mb-8 text-gray-600">
        Welcome to our Shopping Card site! At{" "}
        <span className="font-bold">GOSHOP</span>, we strive to provide you with
        the ultimate online shopping experience. Our platform is built with
        React and Tailwind, ensuring a seamless and visually appealing interface
        that makes your shopping journey enjoyable. We are passionate about
        bringing you a wide range of products from various categories, carefully
        curated to cater to your needs and preferences. Whether you're looking
        for fashion apparel, electronics, home decor, or anything in between,
        we've got you covered. Our team of dedicated professionals works
        tirelessly to ensure that every aspect of your shopping experience is
        top-notch. From user-friendly navigation to secure payment processing,
        we prioritize your convenience and security. Connect with us on
        Instagram and WhatsApp to stay updated with the latest trends, exclusive
        deals, and exciting offers. Simply click on the Instagram and WhatsApp
        icons, located prominently on our website, to be redirected to our
        official profiles. Engage with us, ask questions, and get real-time
        assistance with your purchases.
      </p>

      <div className="mb-5">
        <Logo />
      </div>
      <p className="mb-10 text-gray-600">
        We value your feedback and suggestions, as they help us improve our
        services and make your experience even better. Feel free to reach out to
        us through our contact page or via the provided contact information.
        We're always here to assist you and ensure your complete satisfaction.
      </p>

      <div className="flex items-center justify-center mb-4">
        <span className="mr-5 cursor-pointer">
          <Image
            onClick={openInstagram}
            width={50}
            height={50}
            src={instagram}
            alt="Instagram Icon"
          />
        </span>
        <span className="mr-5 cursor-pointer">
          <Image
            onClick={openWhatsApp}
            width={50}
            height={50}
            src={whatsapp}
            alt="WhatsApp Icon"
          />
        </span>
        <span className="mr-5 cursor-pointer">
          <Image
            onClick={openLinkedIn}
            width={50}
            height={50}
            src={linkedin}
            alt="LinkedIn Icon"
          />
        </span>
      </div>
    </div>
  );
};

export default AboutUsPage;
