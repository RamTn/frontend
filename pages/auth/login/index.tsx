import Authentication from "@/components/authentication";

const SignIn = () => {
  return (
    <div className="fixed bg-gray-50 top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2">
      <Authentication />
    </div>
  );
};

export default SignIn;
