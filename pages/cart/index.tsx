// libraries
import React, { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import { useQuery } from "react-query";
import { useRouter } from "next/router";
import Head from "next/head";
// Apis
import { ApiFactory, ApiTypes } from "@/application";
import CartService from "@/application/cart/cartService";
import { ICart } from "@/domain/cart/cart";
// Components
import Button from "@/components/button/button";
import CartItem from "@/components/cartItem/cartItem";
import { useAppDispatch } from "@/store/hooks";
import { addToCartList } from "@/features/cart/cartSlice";
import Loading from "@/components/loading/loading";

const getCartsData = async ({ queryKey }: any) => {
  const [_, accountUuid] = queryKey;

  return (ApiFactory.createApi(ApiTypes.CART) as CartService).getCarts(
    accountUuid
  );
};

const Cart = () => {
  /*--------------------------- hooks -------------------------*/
  const dispatch = useAppDispatch();
  const router = useRouter();
  const { data: session } = useSession();
  const { data: carts, isLoading } = useQuery(
    ["get-cart", session?.user?.uuid],
    getCartsData,
    {
      enabled: !!session?.user?.uuid,
    }
  );
  const [itemCarts, setItemCarts] = useState(carts?.data ?? []);
  const [selectedCarts, setSelectedCarts] = useState<ICart[]>([]);
  /*------------------------- side effect ----------------------*/
  useEffect(() => {
    setSelectedCarts(carts?.data!);
    setItemCarts(carts?.data!);
  }, [carts?.data]);
  /*----------------------- selected Items ---------------------*/
  const handleToggleItem = (item: ICart) => {
    const foundItem = selectedCarts.find((cart) => cart.uuid === item.uuid);
    if (foundItem) {
      const removedSelected = selectedCarts.filter(
        (selCart) => selCart.uuid !== item.uuid
      );
      setSelectedCarts(removedSelected);
    } else {
      setSelectedCarts([...selectedCarts, item]);
    }
  };
  /*------------------------ delete Items ----------------------*/
  const deletedItem = (uuid: string) => {
    const newItems = itemCarts.filter((itemC) => itemC.uuid !== uuid);
    setItemCarts(newItems);
  };
  /*------------------------ handle to pay ----------------------*/
  const handleGoToPay = () => {
    dispatch(addToCartList(selectedCarts));
    router.push("/payment");
  };
  //------------------------ no item added ----------------------//
  const renderNoItemAdded = () => (
    <div className="fixed top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 flex flex-col w-full items-center justify-center drop-shadow-lg">
      <p className="sm:text-3xl text-2xl text-gray-500 mb-10">
        There is not Item
      </p>
    </div>
  );
  //------------------------ cardsRender ----------------------//
  const cardsRender = () => (
    <>
      <div>
        {itemCarts?.map((item) => (
          <CartItem
            deletedItem={deletedItem}
            key={item.uuid}
            item={item}
            onToggle={handleToggleItem}
          />
        ))}
      </div>
      <div className="flex justify-center w-full mt-10">
        <div className=" w-1/3 md:w-1/5">
          <Button
            disabled={selectedCarts?.length === 0}
            handleClick={handleGoToPay}
            name="Go to Pay"
            outline={false}
            type="button"
          />
        </div>
      </div>
    </>
  );
  /*------------------------------------------------------------*/
  return (
    <>
      <Head>
        <title>GOSHOP - Carts</title>
      </Head>
      <h1 className="text-gray-500 font-bold my-5 ml-[17px]  text-sm md:text-lg sm:text-sm lg:text-3xl">
        Carts
      </h1>
      {isLoading ? (
        <Loading />
      ) : (
        <div className="container mx-auto p-4">
          {!itemCarts?.length ? renderNoItemAdded() : cardsRender()}
        </div>
      )}
    </>
  );
};

export default Cart;
