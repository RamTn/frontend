// libraries
import { useEffect } from "react";
import { GetServerSidePropsContext } from "next";
import { getSession } from "next-auth/react";
// Apis
import { ApiFactory, ApiTypes } from "@/application";
import FavouriteService from "@/application/favourite/favouriteService";
import { IProduct } from "@/domain/product/product";
// Components
import Card from "@/components/card/card";
import { getListFavourite } from "@/features/favourite/favouriteSlice";
import { useAppDispatch, useAppSelector } from "@/store/hooks";
import Head from "next/head";

interface IfavouriteProps {
  favourites: IProduct[];
}

const Favourite: React.FC<IfavouriteProps> = ({ favourites }) => {
  //----------------------- Hooks -----------------------//
  const dispatch = useAppDispatch();
  const { favouritesList = [] } = useAppSelector((state) => state.favourite);
  //---------------------- Side efect -------------------//
  useEffect(() => {
    dispatch(getListFavourite(favourites));
  }, [dispatch]);
  //-------------------- no item added ------------------//
  const renderNoItemAdded = () => (
    <div className="fixed top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 flex flex-col w-full items-center justify-center drop-shadow-lg">
      <p className="sm:text-3xl text-2xl text-gray-500 mb-10">
        There is not Item
      </p>
    </div>
  );
  //-----------------------------------------------------//
  return (
    <>
      <Head>
        <title>GOSHOP - Favourites</title>
      </Head>
      <h1 className="text-gray-500 font-bold my-5 ml-[17px]  text-sm md:text-lg sm:text-sm lg:text-3xl">
        Favourites
      </h1>
      <div className="flex flex-wrap sm:justify-around md:justify-start lg:justify-start justify-around ">
        {!favouritesList?.length
          ? renderNoItemAdded()
          : favouritesList.map((favourite) => (
              <Card key={favourite.uuid} {...favourite}>
                <Card.Image />
                <Card.Title />
                <Card.Desrciption />
                <Card.StarRating />
                <Card.ButtonCard />
              </Card>
            ))}
      </div>
    </>
  );
};

export default Favourite;

export async function getServerSideProps(contex: GetServerSidePropsContext) {
  const session = await getSession(contex);

  const favouriteResponse = await (
    ApiFactory.createApi(ApiTypes.FAVOURITE) as FavouriteService
  ).getAllFavouritesByAccountUuid(
    session?.user?.uuid!,
    session?.user?.access_token!
  );

  return {
    props: {
      favourites: favouriteResponse.data,
      error: [favouriteResponse.error],
    },
  };
}
