// libraries
import Head from "next/head";
import { getSession } from "next-auth/react";
import { GetServerSidePropsContext } from "next";
// Apis
import { ApiFactory, ApiTypes } from "@/application";
import { ICategory } from "@/domain/category/category";
import CategoryService from "@/application/category/categoryService";
import ProductService from "@/application/product/productService";
import { IProduct } from "@/domain/product/product";
// Components
import Category from "@/components/category/category";
import Card from "@/components/card/card";

interface IHomeProps {
  categories: ICategory[];
  populars: IProduct[];
}

export default function Home({ categories, populars }: IHomeProps) {
  return (
    <>
      <Head>
        <title>GOSHOP | Best Deals on Quality Products</title>
        <meta
          name="description"
          content="Explore a wide range of high-quality products on our shopping site. Find great deals on electronics, fashion, home decor, and more."
        />
      </Head>
      <Category categories={categories} />
      <h1 className="text-gray-500 font-bold mt-16  text-sm md:text-lg sm:text-sm lg:text-3xl">
        Populars
      </h1>
      <div className="flex flex-wrap sm:justify-around md:justify-start lg:justify-start justify-around ">
        {populars.map((popular) => (
          <Card key={popular.uuid} {...popular}>
            <Card.Image />
            <Card.Title />
            <Card.Desrciption />
            <Card.StarRating />
            <Card.ButtonCard />
          </Card>
        ))}
      </div>
    </>
  );
}

export async function getServerSideProps(contex: GetServerSidePropsContext) {
  const session = await getSession(contex);

  const [categoriesResponse, popularProductsResponse] = await Promise.all([
    (
      ApiFactory.createApi(ApiTypes.CATEGORY) as CategoryService
    ).getCategories(),
    (ApiFactory.createApi(ApiTypes.PRODUCT) as ProductService).getPopulars(
      4,
      session?.user?.uuid
    ),
  ]);

  return {
    props: {
      categories: categoriesResponse.data,
      populars: popularProductsResponse.data,
      error: [categoriesResponse.error, popularProductsResponse.error],
    },
  };
}
