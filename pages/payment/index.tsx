// libraries
import React, { useState, useRef } from "react";
import Image from "next/legacy/image";
import { useEffect } from "react";
import { FaRegAddressCard, FaCity } from "react-icons/fa";
import { AiOutlinePhone, AiOutlineCodeSandbox } from "react-icons/ai";
import { ImEarth } from "react-icons/im";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { useSession } from "next-auth/react";
import { useQuery } from "react-query";
import { BsPlusCircleFill } from "react-icons/bs";
// Apis
import { ApiFactory, ApiTypes } from "@/application";
import AddressService from "@/application/address/addressService";
import { IAddress } from "@/domain/address/address";
import CartService from "@/application/cart/cartService";
// Components
import Button from "@/components/button/button";
import { useAppSelector } from "@/store/hooks";
import Input from "@/components/input/input";
import Loading from "@/components/loading/loading";
import Address from "@/components/address/address";
import Head from "next/head";

type TInputs = {
  address: string;
  phone: string;
  country: string;
  state: string;
  postalCode: string;
};

const getAddressesData = async ({ queryKey }: any) => {
  const [_, accountUuid] = queryKey;

  return (
    ApiFactory.createApi(ApiTypes.ADDRESS) as AddressService
  ).getAddresses(accountUuid);
};

const Payment = () => {
  /*---------------------------- hooks ---------------------------- */
  const router = useRouter();
  const onceReturn = useRef(1);
  const [addressesState, setAddresses] = useState<IAddress[]>([]);
  const [isShowAddressForm, setIsShowAddressForm] = useState(false);
  const [activateAddressUuid, setActivateAddressUuid] = useState("");
  const cart = useAppSelector((state) => state.cart);
  const { data: session } = useSession();
  const { data: addresses, isLoading } = useQuery(
    ["get-addresses", session?.user?.uuid],
    getAddressesData,
    { enabled: !!session?.user.uuid }
  );
  /*-------------------------- Side effects ------------------------ */
  useEffect(() => {
    if (cart.cartsList.length === 0) {
      if (onceReturn.current === 1) {
        onceReturn.current = 2;
        router.push({ pathname: "/cart" }, undefined, {
          shallow: true,
        });
      }
    }
  }, [cart.cartsList]);

  useEffect(() => {
    const foundActiveAddress = addressesState?.find(
      (item) => item.active === true
    )?.uuid;
    if (foundActiveAddress) {
      setActivateAddressUuid(foundActiveAddress);
    }
  }, [addressesState]);

  useEffect(() => {
    setAddresses(addresses?.data!);
  }, [addresses?.data]);

  /*-------------------------- addresses ------------------------ */
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<TInputs>();

  const onSubmit = (data: TInputs) => {
    if (session?.user.access_token) {
      (ApiFactory.createApi(ApiTypes.ADDRESS) as AddressService)
        .addAddress({
          accountUuid: session?.user?.uuid!,
          ...data,
        })
        .then((response) => {
          setAddresses(response.data);
          setIsShowAddressForm(false);
        });
    }
  };

  const getActivateAddressUuid = (uuid: string) => {
    setActivateAddressUuid(uuid);
  };

  const showAddressesForm = () => {
    setIsShowAddressForm((mode) => !mode);
  };

  const deleteAddress = (uuid: string) => {
    setAddresses((value) => value.filter((item) => item.uuid !== uuid));
  };

  const addressForm = () => {
    return (
      <div className="flex flex-col items-center">
        <form
          className="w-11/12  p-8"
          onSubmit={handleSubmit(onSubmit)}
          noValidate
        >
          <Input
            error={errors?.address?.message}
            register={register("address", {
              required: "address is required",
            })}
            type="text"
            label="Address"
            IconComponent={FaRegAddressCard}
            placeholder="Enter your Address"
          />
          <Input
            error={errors?.phone?.message}
            register={register("phone", {
              required: "phone is required",
            })}
            type="text"
            label="Phone"
            IconComponent={AiOutlinePhone}
            placeholder="Enter your Phone"
          />
          <div className="lg:flex  lg:justify-between">
            <Input
              error={errors?.country?.message}
              register={register("country", {
                required: "country is required",
              })}
              type="text"
              label="Country"
              IconComponent={ImEarth}
              placeholder="Enter your country"
            />
            <Input
              error={errors?.state?.message}
              register={register("state", {
                required: "state is required",
              })}
              type="text"
              label="State"
              IconComponent={FaCity}
              placeholder="Enter your State"
            />
            <Input
              error={errors?.postalCode?.message}
              register={register("postalCode", {
                required: "postalCode is required",
              })}
              type="text"
              label="PostalCode"
              IconComponent={AiOutlineCodeSandbox}
              placeholder="Enter your postalCode"
            />
          </div>
          <div className="mt-10 sm:w-1/4 m-auto w-1/3">
            <Button
              outline={false}
              handleClick={handleSubmit(onSubmit)}
              name="Add"
              type="submit"
            />
          </div>
        </form>
      </div>
    );
  };
  /*-------------------------- carts ------------------------ */
  const cartsRender = () => {
    return (
      <div>
        {cart.cartsList?.map((item) => (
          <div key={item.uuid} className="flex items-center">
            <div className="flex justify-between  drop-shadow-lg bg-gray-50 items-center rounded-md mb-4 w-full">
              <div className="relative flex aspect-square w-32 flex-col items-center justify-center rounded-md">
                <Image
                  src={item?.image[0]}
                  priority={true}
                  alt="cart-image"
                  layout="fill"
                  sizes="20"
                  className="absolute rounded-lg"
                />
              </div>
              <h4 className="hidden sm:block w-56 truncate">{item.name}</h4>
              <h2>{item.totallPrice} $</h2>
              <h2 className="mr-5">{item.quantity}</h2>
            </div>
          </div>
        ))}
      </div>
    );
  };
  /*----------------------- Pay render ---------------------- */
  const handlePay = () => {
    if (router.query.name === "product") {
      router.push("/suceed-payment");
    } else {
      (ApiFactory.createApi(ApiTypes.CART) as CartService)
        .deleteCarts(cart.cartsList.map((item) => item.uuid))
        .then((_) => {
          router.push("/suceed-payment");
        });
    }
  };

  const price = cart.cartsList.reduce(
    (totall, num) => totall + num.totallPrice,
    0
  );

  const renderPayment = () => (
    <div className="flex justify-between w-full mt-10">
      <div className="text-gray-500 text-xl">totall: {price} $</div>
      <div className="w-1/5">
        <Button
          handleClick={handlePay}
          name="Pay"
          outline={false}
          type="button"
        />
      </div>
    </div>
  );

  /*-------------------------------------------------------- */
  return (
    <>
      <Head>
        <title>GOSHOP - Payment</title>
      </Head>
      {isLoading ? (
        <Loading />
      ) : (
        <div className="container mx-auto p-4">
          {cartsRender()}
          <div className="flex items-center mt-10">
            <h1 className="pr-5 text-2xl text-gray-600 font-bold">Addresses</h1>
            <BsPlusCircleFill
              onClick={showAddressesForm}
              className="text-teal-400 cursor-pointer"
              size={30}
            />
          </div>
          {isShowAddressForm ? addressForm() : ""}
          {addressesState?.length
            ? addressesState.map((ad) => (
                <Address
                  address={ad}
                  getActivateAddressUuid={(uuid) =>
                    getActivateAddressUuid(uuid)
                  }
                  deleteAddress={(uuid) => deleteAddress(uuid)}
                  key={ad.uuid}
                  activateAddressUuid={activateAddressUuid}
                />
              ))
            : ""}
          {renderPayment()}
        </div>
      )}
    </>
  );
};

export default Payment;
