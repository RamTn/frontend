// libraries
import React from "react";
import { GetServerSidePropsContext } from "next";
import { getSession } from "next-auth/react";
import Head from "next/head";
// Apis
import { ApiFactory, ApiTypes } from "@/application";
import { IProduct } from "@/domain/product/product";
import ProductService from "@/application/product/productService";
// Components
import ProductDetail from "@/components/product/product";

interface IProductPage {
  product: IProduct;
}

const Product: React.FC<IProductPage> = ({ product }) => {
  return (
    <div className="mb-10">
      <Head>
        <title>{product?.name}</title>
        <meta name="description" content={product?.description} />
      </Head>
      {product ? (
        <ProductDetail {...product}>
          <ProductDetail.Image />
          <ProductDetail.Detail>
            <ProductDetail.DetailTitle />
            <ProductDetail.DetailPrice />
            <ProductDetail.DetailDescription />
            <ProductDetail.DetailPath />
            <ProductDetail.DetailStarRating />
            <ProductDetail.DetailQuantity />
            <ProductDetail.DetailActions />
          </ProductDetail.Detail>
        </ProductDetail>
      ) : (
        ""
      )}
    </div>
  );
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const { params } = context;
  const session = await getSession(context);
  const uuid = params?.productUuid as string;

  const productResponse = await (
    ApiFactory.createApi(ApiTypes.PRODUCT) as ProductService
  ).getProductByUuid(uuid, session?.user?.uuid);

  return {
    props: {
      product: productResponse.data,
      error: [productResponse.error],
    },
  };
}

export default Product;
