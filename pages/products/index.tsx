// libraries
import React, { useEffect, useMemo, useState } from "react";
import { GetServerSidePropsContext } from "next";
import InfiniteScroll from "react-infinite-scroll-component";
import { useRouter } from "next/router";
import { useSession, getSession } from "next-auth/react";
import { useQuery } from "react-query";
import Head from "next/head";
// Apis
import { ApiFactory, ApiTypes } from "@/application";
import { IProduct } from "@/domain/product/product";
import ProductService from "@/application/product/productService";
// Components
import Card from "@/components/card/card";
import Filter from "@/components/filter/filter";

interface IProductsProps {
  products: IProduct[];
}

const Products: React.FC<IProductsProps> = ({ products }) => {
  const [productQuery, setProductQuery] = useState<any>("");
  const { data: session } = useSession();

  const getProducts = async ({ queryKey: [_key, productQuery] }: any) => {
    return (
      ApiFactory.createApi(ApiTypes.PRODUCT) as ProductService
    ).getAllProducts({ ...productQuery, accountUuid: session?.user?.uuid });
  };
  const { data, refetch } = useQuery(["product", productQuery], getProducts, {
    enabled: false,
  });
  /*----------------------------- states --------------------------------*/
  const router = useRouter();
  const [productsState, setProductsState] = useState<IProduct[]>(products);
  const [page, setPage] = useState<number>(1);
  const [hasMore, setHasMore] = useState<boolean>(true);
  /*-------------------------- memorization -----------------------------*/
  const memoizedProducts = useMemo(() => productsState, [productsState]);
  /*----------------------------- effects --------------------------------*/
  useEffect(() => {
    if (products.length >= 0 && products.length < 9) {
      setHasMore(false);
    }
  }, []);

  useEffect(() => {
    if (productQuery) refetch();
  }, [productQuery]);

  useEffect(() => {
    if (data) {
      if (data?.data.length === 0) {
        setHasMore(false);
      }

      setProductsState([...productsState, ...data?.data!]);
    }
  }, [data]);

  /*----------------------- get Products from filter --------------------*/
  const handleGetProductsFromFilter = (productsFilter: IProduct[]) => {
    setProductsState(productsFilter);
    productsFilter.length < 9 ? setHasMore(false) : setHasMore(true);
    setPage(1);
  };
  /*----------------------- add next products ---------------------------*/
  const nextProducts = async () => {
    setPage((page) => page + 1);

    const queryWithNextPage = { ...router.query, page: String(page + 1) };
    setProductQuery(queryWithNextPage);
  };
  /*--------- render loader and End message for products fetching --------*/
  const messageStyle = "flex justify-center mb-4 text-2xl text-gray-500";

  const loadingMessage = () => {
    return <p className={messageStyle}>Loading ...</p>;
  };
  const endOfProducts = () => {
    return <p className={messageStyle}></p>;
  };
  /*---------------------------------------------------------------------*/
  return (
    <>
      <Head>
        <title>Best Products for Your Needs | GOSHOP</title>
        <meta
          name="description"
          content="Explore our wide range of high-quality products. Find the best deals on fashion."
        />
      </Head>
      <InfiniteScroll
        dataLength={productsState.length}
        next={nextProducts}
        hasMore={hasMore}
        loader={loadingMessage()}
        endMessage={endOfProducts()}
      >
        <Filter
          getClientProduct={(productsFilter: IProduct[]) =>
            handleGetProductsFromFilter(productsFilter)
          }
        />
        <div className="flex flex-wrap sm:justify-around md:justify-start lg:justify-start justify-around ">
          {memoizedProducts.map((product) => (
            <Card key={product.uuid} {...product}>
              <Card.Image />
              <Card.Title />
              <Card.Desrciption />
              <Card.StarRating />
              <Card.ButtonCard />
            </Card>
          ))}
        </div>
      </InfiniteScroll>
    </>
  );
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const { query } = context;
  const session = await getSession(context);

  const productsResponse = await (
    ApiFactory.createApi(ApiTypes.PRODUCT) as ProductService
  ).getAllProducts({ ...query, accountUuid: session?.user?.uuid });

  return {
    props: {
      products: productsResponse.data,
      error: [productsResponse.error],
    },
  };
}

export default Products;
