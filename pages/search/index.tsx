// libraries
import React, { useEffect, useMemo, useState } from "react";
import { GetServerSidePropsContext } from "next";
import { useRouter } from "next/router";
import { useSession, getSession } from "next-auth/react";
import InfiniteScroll from "react-infinite-scroll-component";
import Head from "next/head";
// Apis
import { ApiFactory, ApiTypes } from "@/application";
import ProductService from "@/application/product/productService";
import { IProduct } from "@/domain/product/product";
// Components
import Card from "@/components/card/card";
import SearchProduct from "@/components/searchProduct/searchProduct";

interface ISearchProps {
  products: IProduct[];
}

const Search: React.FC<ISearchProps> = ({ products }) => {
  /*----------------------------- hooks --------------------------------*/
  const router = useRouter();
  const [productsState, setProductsState] = useState<IProduct[]>(products);
  const [page, setPage] = useState<number>(1);
  const [hasMore, setHasMore] = useState<boolean>(true);

  const { data: session } = useSession();

  const memoizedProducts = useMemo(() => productsState, [productsState]);
  /*----------------------------- effects --------------------------------*/
  useEffect(() => {
    if (products.length >= 0 && products.length < 9) {
      setHasMore(false);
    }
  }, []);
  /*---------------------- get Products from search --------------------*/
  const handleGetProductsFromFilter = (productsFilter: IProduct[]) => {
    setProductsState(productsFilter);
    productsFilter.length < 9 ? setHasMore(false) : setHasMore(true);
    setPage(1);
  };
  /*--------- render loader and End message for products fetching --------*/
  const loadingMessage = () => {
    return (
      <p className="flex justify-center mb-4 text-2xl text-gray-500">
        Loading ...
      </p>
    );
  };
  const endOfProducts = () => {
    return <p className="flex justify-center mb-4 text-2xl text-gray-500"></p>;
  };
  /*----------------------- add next products ---------------------------*/
  const nextProducts = async () => {
    setPage((page) => page + 1);

    const queryWithNextPage = {
      ...router.query,
      page: String(page + 1),
      accountUuid: session?.user?.uuid,
    };
    const productsResponse = await (
      ApiFactory.createApi(ApiTypes.PRODUCT) as ProductService
    ).getAllProducts(queryWithNextPage);

    if (productsResponse.data.length === 0) {
      setHasMore(false);
    }

    setProductsState([...productsState, ...productsResponse.data]);
  };
  /*---------------------------------------------------------------------*/
  return (
    <>
      <Head>
        <title>Best Products for Your Needs | GOSHOP</title>
        <meta
          name="description"
          content="Explore our wide range of high-quality products. Find the best deals on fashion."
        />
      </Head>
      <InfiniteScroll
        dataLength={productsState.length}
        next={nextProducts}
        hasMore={hasMore}
        loader={loadingMessage()}
        endMessage={endOfProducts()}
      >
        <div className="flex justify-center w-full">
          <div className="w-5/6 md:3/5 lg:w-1/2">
            <SearchProduct
              getClientProduct={(productsFilter: IProduct[]) =>
                handleGetProductsFromFilter(productsFilter)
              }
            />
          </div>
        </div>
        <div className="flex flex-wrap sm:justify-around md:justify-start lg:justify-start justify-around ">
          {memoizedProducts.map((product) => (
            <Card key={product.uuid} {...product}>
              <Card.Image />
              <Card.Title />
              <Card.Desrciption />
              <Card.StarRating />
              <Card.ButtonCard />
            </Card>
          ))}
        </div>
      </InfiniteScroll>
    </>
  );
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const { query } = context;
  const session = await getSession(context);

  const productsResponse = await (
    ApiFactory.createApi(ApiTypes.PRODUCT) as ProductService
  ).searchProductsByName({ ...query, accountUuid: session?.user?.uuid });

  return {
    props: {
      products: productsResponse.data,
      error: [productsResponse.error],
    },
  };
}

export default Search;
