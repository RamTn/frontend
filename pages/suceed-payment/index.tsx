// libraries
import React from "react";
import { useRouter } from "next/router";
// Components
import Button from "@/components/button/button";
import Head from "next/head";

function SuceedPayment() {
  const router = useRouter();
  return (
    <div className="relative">
      <Head>
        <title>GOSHOP - Success payment</title>
      </Head>
      <div className="fixed top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 flex flex-col w-full items-center justify-center drop-shadow-lg">
        <h1 className="sm:text-3xl text-2xl text-gray-500 mb-10">
          Your payment has been successful
        </h1>
        <div className="sm:w-1/4 w-3/4">
          <Button
            outline={false}
            name="Return to Home"
            type="button"
            handleClick={() => router.push("/")}
          />
        </div>
      </div>
    </div>
  );
}

export default SuceedPayment;
