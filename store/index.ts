import { configureStore } from "@reduxjs/toolkit";
import generalSlice from "@/features/general/generalSlice";
import favouriteSlice from "@/features/favourite/favouriteSlice";
import cartSlice from "@/features/cart/cartSlice";

export const store = configureStore({
  reducer: {
    general: generalSlice,
    favourite: favouriteSlice,
    cart: cartSlice,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
