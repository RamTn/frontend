import NextAuth from "next-auth";

declare module "next-auth" {
  interface Session {
    user: {
      userName: string;
      name: string;
      email: string;
      address: string;
      role: string;
      access_token: string;
      uuid: string;
    };
  }
}
